"""This is a program to create pictures of phone numbers.

    To see the graph of a phone number, just enter it in the text box at the
top and press enter(or click on Show Me!).
    The constants and options work as follows:
     * SuperScale lets you modifiy the size of the graph,
     * LineWidth the width of the line,
     * salting turns on a bit of random jiggle for lines that start or end in the
         same place,
     * coloring makes each line a random color.

    Created by Jesse Weinstein.  Released on Feb 19, 2001"""
from Tkinter import *
import random, string, re
class PhoneDrawingMaker:
    #Tuneable Constants
    SuperScale=4
    LineWidth=1

    #Boolen options
    salting=0
    coloring=0

    DEBUG=0
      #Interesting number to try:  
      #(397) 426-8413                             
                                   
    
    def __init__(self):
        """Set a few generated or non-tuneable constants and call make_dialog."""
        self.PhoneKeyPad = {'1':(1,1), '2':(2,1), '3':(3,1), \
                            '4':(1,2), '5':(2,2), '6':(3,2), \
                            '7':(1,3), '8':(2,3), '9':(3,3), \
                            '*':(1,4), '0':(2,4), '#':(3,4)}
        self.Ltrs2NumsTbl=string.maketrans(\
            string.uppercase, '2'*3+'3'*3+'4'*3+'5'*3+'6'*3+'7'*4+'8'*3+'9'*4)
        self.scale=18*self.SuperScale
        self.make_dialog()
        
    def make_dialog(self):
        """Create and display the interface."""
        self.master=Tk()
        self.master.title('Phone Graphing Tool')
        Label(self.master, text="Phone Number:").grid(row=0)
        self.e = Entry(self.master)
        self.e.grid(row=0,column=1)
        self.c = Canvas(self.master, height=75*self.SuperScale, \
                        width=60*self.SuperScale, relief=RIDGE, bd='.02i')
        self.c.grid(row=1, column=1)
        self.b = Button(self.master, text="Show me!", command=self.show_me)
        self.b.grid(row=2, column=1)
##        self.err = Label(self.master, wraplength='.6i') #text="No Error! XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX")
##        self.err.grid(row=1)
        self.master.bind('<Return>', self.show_me)

    def show_me(self, event=None):
        """Callback: Fixup and draw the phone number."""
        self.fixup()
        if not self.PhoneStr:
            return
        else:
            self.draw()

    def fixup(self):
        """Fixup the string by translating letters to numbers,
        and removing anything else.  Also push the translated string back to the
        interface."""
        self.PhoneStr=string.translate(self.e.get().upper(), self.Ltrs2NumsTbl)
        self.e.delete(0, END)
        self.e.insert(0, self.PhoneStr)
        self.PhoneStr=re.sub('[^0123456789*#]', '', self.PhoneStr)
    
    def draw(self):
        """Do the actual drawing.

            This is done using the PhoneKeyPad dict; it maps numbers to
        locations on a phone keypad.  Then after checking for and processing
        salting and coloring options, a line just has to be drawn between each
        point returned from PhoneKeyPad."""
        self.c.delete(ALL)
        pX=self.PhoneKeyPad[self.PhoneStr[0]][0]*self.scale
        pY=self.PhoneKeyPad[self.PhoneStr[0]][1]*self.scale
        if self.DEBUG:
            print
            print 'The Coords of each line created(without salt):'
            print '--------------------------------'
        for number in list(self.PhoneStr):
                if self.DEBUG:
                    print pX,pY,'|', self.PhoneKeyPad[number][0]*self.scale,\
                          self.PhoneKeyPad[number][1]*self.scale

                salt = 0
                if list(self.PhoneStr).count(number) > 1 and self.salting:
                    salt=random.randint(-2,2)

                color = "black"
                if self.coloring:
                    color = "#%02x%02x%02x" % (random.randint(0,255), random.randint(0,255), random.randint(0,255))

                self.c.create_line(pX,pY, \
                                   self.PhoneKeyPad[number][0]*self.scale+salt, \
                                   self.PhoneKeyPad[number][1]*self.scale+salt, \
                                   fill=color, width=self.LineWidth)
                pX=self.PhoneKeyPad[number][0]*self.scale+salt
                pY=self.PhoneKeyPad[number][1]*self.scale+salt

It=PhoneDrawingMaker()
