#.wdb parser by Jesse Weinstein <jessw@netwood.net>
#Description of it at http://en.wikipedia.org/wiki/User:JesseW/wdb_format
#Released Thu May 25 09:43:53 2006
#License: MIT license ( http://opensource.org/licenses/mit-license.html )
#Copyright (c) 2006 Jesse Weinstein

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

#Version 0.7
#Changelog:
#0.5.1 - Made the removal of the header-like thing in the middle of the
#        file optional, as some files seem not to have it.
#        Bug reported by David Kreindler.
#0.5.2 - Lots more comments; more debuging info; maybe a solution to
#        some odd bits in a file from David Kreindler.
#0.6 -   Greatly re-written; it didn't handle numbers before, now it handles
#        *most* of them correctly.
#0.6.1 - Added handling of continuation lines; fixed printit to handle
#        blank lines (which often show up when things are misunderstood)
#0.6.1.1 - quick change to make a Exception into a simple, do no more "return" clause.
#        kludge because I don't understand how to avoid parsing the footer, yet.
#0.7 -   Now handles integers expressed with the [T starting header; decimals still don't work.

#I'm beginning to guess that data byte 4(byte 8) is an index into a style table in some header...
#And byte 3 is sometimes \x01, not sure why (doesn't seem to be connected to continuation lines, thought it might)

#header interesting bits:
#byte 516 - varies; seen hex(ord(txt[516])) in ['0x5c','0x5d','0x04']

import sys, struct, os
class WDBparser:

    def __init__(self, txt, ext):
        self.txt=txt
        self.ext=ext
        self.items=[]
        self.data={}
        #A list of indexes into self.items listing items with strange headers
        self.badItems=[]
        self.ints=[]
    def parse(self):
        #print self.ext
        if self.ext=='.wdb':
            self.body=self.txt[self.txt.index('\x25\x00\xf2')+246:] #Remove header
        elif self.txt.count('\x03\x00\x01\x00\x05\x00'):
            self.body=self.txt[self.txt.index('\x03\x00\x01\x00\x05\x00')+6:]
        else:
            self.body=self.txt[751:]
        current=[]
        p=self.body.find('\xffT') #Remove 2nd header-like object
        if p!=-1: 
            self.body=self.body[:p]+self.body[p+4096:]
            
        if self.ext=='.wdb':
            p=self.body.rfind('\x1b\x00\x08\x00') #Remove footer
        else:
            p=self.body.rfind('\x01\x00\x00\x00')
        if p!=-1:
             self.body=self.body[:p]
        
        #Go char by char; make self.items
        for theindex in range(len(self.body)):
            n=self.body[theindex]
            #print '%%%%', `n`, current
            if not current:
                #print '@@@@'
                if n in ['\x0f', '\x0e', '[', '6', '\x0c', '\x10']:
                    current.append(n)
                else:
                    warning('ERROR: Unknown header type (Stopping): ', theindex,n, \
                            self.body[theindex-30:theindex+30])
                    return
            else:
                if (current[0]=='\x0f' and (len(current)<10 or n!='\x00')) or \
                   (current[0]=='\x0e' and len(current)<17) or \
                   (current[0]=='['    and len(current)<13) or \
                   (current[0]=='6'    and (len(current)<10 or n!='\x00')) or \
                   (current[0]=='\x0c' and len(current)<9) or \
                   (current[0]=='\x10' and (len(current)<31 or n!='\x03')) or \
                   0:
                    current.append(n)
                else:
                    self.items.append(''.join(current)+n)
                    current=[]
    def parse2(self):
        #Go item(created in above loop) by item; make self.data
        for N in range(len(self.items)):
            n=self.items[N]
            if not (ord(n[3])<2 and n[5]==n[9]=='\x00' and ord(n[8])<8 and (n[0]!='[' or n[:2]=='[T')):
                self.badItems.append(N)
            rn=ord(n[6])+(ord(n[7])*256)#record number
            fn=ord(n[4]) #field number
            q=self.data.setdefault(rn, {})
            if n[0]=='6':
                q[fn]+=n[10:-1]
            elif q.has_key(fn):
                warning('WARNING: field is repeated; ',\
                        N, rn, fn, q, ''.join(n))
            if ord(n[7])>1:
                warning('WARNING: record # is oddly large; ',\
                        N, rn, fn, q, ''.join(n))
            if n[0]=='\x0f':
                q[fn]=n[10:-1]
            if n[0]=='\x0e':
                q[fn]=('%.5f' % struct.unpack('<d',n[10:])[0])
            if n[0]=='[' and n[1]=='T':
#                print '!###!!!', n
                self.ints.append(n[10:])
                n2=struct.unpack('<i',n[10:])[0]
                q[fn]=`n2 and (struct.unpack('f', struct.pack('i',(((n2 & 2146435072) - 939524096)<<3) | ((n2 & 1048575) <<3) | (n2 & -2147483648)))[0])`
#                print '!!!!!!!!!!!!!!!!!!!', q[fn]
        if self.badItems:
            warning('WARNING: items with odd headers:', \
                    self.badItems, [self.items[n] for n in self.badItems])
    def printit(self):
        mf=max([max(x.keys()) for x in self.data.values()])
        mr=max(self.data.keys())
        ans=[]
        for n in range(mr+1):
            ans.append(['']*(mf+1))
            if self.data.has_key(n):
                for x in self.data[n].items():
                    ans[-1][x[0]]=x[1]
            else:
                warning('WARNING: blank or otherwise missing line: (out of: '+`mr`+' lines.)', n)
            ans[-1]='"'+('","'.join(ans[-1]))+'"'
        return '\n'.join(ans)
def warning(message, *args):
    sys.stderr.write(message+('; '.join([`x` for x in args]))+'\n')

if __name__=='__main__':
    import sys
    txt=open(sys.argv[1], 'r').read()
    #print txt.index('[T\n\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
    if len(sys.argv)>3 and sys.argv[2]=='-t':
        print repr(txt)
    else:
        it=WDBparser(txt,os.path.splitext(sys.argv[1])[1].lower())
        it.parse();it.parse2()
        print it.printit()
        #print it.ints

#[0, 240, 0, 8, 16, 20, 24, 28, 32, 34, 36]
#[0, 1,   2, 3,  4,  5,  6,  7,  8,  9, 10]

#(x-2^(8+14))/2^14

#[int(n,16)/2**11-(2**9+2**10+2**11) for n in q44r] #makes thousands work
#[int(n,16)/2**14-2**8 for n in q44r] #makes 100s work
"""
0000
0001
0010
0011
0100
0101 5
0110
0111
1000
1001
1010 10  a
1011     b
1100     c
1101     d
1110     e
1111 15  f

'     c2         ed        40   00'
|1|100 0010 1|110 1101 0100 0000 0000 0000|
|1|100 0010 1|110 1101 0100 0000 0000 0000

'0040edc2'
0000 0000 0100 0000 1110 1101 1100 0010
  7   8     5   6     3   4    1    2  
['00000000',
 '  00        00        f0        3f',
0000 0000 0000 0000 1111 0000 0011 1111
                     3    4    1    2  
 '00000040',
 '00000840',
 '00001040']

118.625

 
1100 0010  1110 1101  0100 0000  0000 0000
"""

#struct.unpack('f', struct.pack('i',((((struct.unpack('<i',n)[0] & str2dec('\x7f\xf0\x00\x00')) >>20)-1023+127)<<23) | ((struct.unpack('<i',n)[0] & str2dec('\x00\x0f\xff\xff'))<<3)))
