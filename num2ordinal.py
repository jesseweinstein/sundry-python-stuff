#Written by Jesse Weinstein <jessw@netwood.net>.
#Released under the Python license on Sat Jan 22 01:58:46 2005.
#num2ordinal.py
#Return given integer as an ordinal, i.e. 11th, 23rd, 69th, etc.
#Python version: er, 1.5.2 or later, I think.  I've tried it on 2.3...
#http://www.netwood.net/usr/jessw
#Possible categories: Math, Strings
#First release.
#Released to Useless Python (http://www.uselesspython.com) and my site.

def num2ordinal(num):
    """Return given integer as an ordinal, i.e. 123rd, 42nd, etc."""
    return `num`+((num/10)==1 and 'th' or (['st', 'nd', 'rd']+(['th']*6))[(num % 10)-1])
