pv -lpcbraN 'ids' < $1 |
  ./ia-mine-0.5-py3.4.pex --workers 600 --retries 30 2> error_messages |
  pv -tlacbrN 'mine' |
  parallel --pipe --group --block 5M jq -c -f make_census_data.jq |
  pv -lacbrN 'parse' > $2
