# Second version of the census data processing script.
# by Jesse W
# Based on https://gist.github.com/jjjake/161b318d9d5114051cd6#file-get_file_size_md-jq

# Assign metadata object to variable for use in final output.
.metadata as $m |

# Filter out any items that do not have files metadata.
if .files | not then 
    select(.dir?) | {"id": .dir | split("\/")[3], is_dark} 
else 

# Get all non-derivative files that have a file size, and slim down the metadata.
.files |
map(
    select(.source != "derivative" and .name != "\($m.identifier)_files.xml") |
    {name, format, md5, sha1,
     # if case for catching files with size=null (i.e. files.xml).
     "size": (.size // 0 | tonumber), 
     } + (if .private? then {private} else {} end)
) |

# Get total size of files (per item).
(map(.size) | reduce .[] as $item (0; . + $item)) as $ts |


# Final output (per item).
{"id": $m.identifier, "collection": $m.collection, "publicdate": $m.publicdate, 
 "total_size": $ts, "files": .}
+ (if [.[] | .private?] | any then {"some_private": "true"} else {} end)
+ (if $m.noindex? then {"noindex": $m.noindex} else {} end)
end
