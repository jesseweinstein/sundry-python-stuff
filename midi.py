#add more Event Handlers and try to deal with the VLQ parser problem.
#also fix parse_file so it works with the MidiFileClass class.
import string, struct, types
from Tkinter import *
class MidiFileObject:
    """This is a class that represents a MIDI file."""
    def __init__(self, file_object):
        self.f=file_object
        #Handler lists:
        self.MidiSystemEvents={
            0xFF: self.Meta_SystemEvent}
            
        self.MidiVoiceEvents={
            0x08: self.NoteOff_VoiceEvent,
            0x09: self.NoteOn_VoiceEvent,
            0x0B: self.Controller_VoiceEvent,
            0x0C: self.PatchChange_VoiceEvent,
            0x0D: self.ChannelPress_VoiceEvent,
            0x0E: self.PitchWheel_VoiceEvent}

        self.MetaEvents={
            0x00: self.seqNum_MetaEvent,
            0x01: self.text_MetaEvent,
            0x02: self.text_MetaEvent,
            0x03: self.text_MetaEvent,
            0x04: self.text_MetaEvent,
            0x05: self.text_MetaEvent,
            0x06: self.text_MetaEvent,
            0x07: self.text_MetaEvent,
            0x20: self.channel_MetaEvent,
            0x21: self.port_MetaEvent,
            0x2F: self.endTrk_MetaEvent,
            0x51: self.tempo_MetaEvent,
            0x54: self.SMPTE_MetaEvent, 
            0x58: self.timeSig_MetaEvent,
            0x59: self.keySig_MetaEvent,
            0x7F: self.text_MetaEvent}
        
        self.TextTypes={
            0x01: 'Text Meta Event: ',
            0x02: 'Copyright Meta Event: ',
            0x03: 'Track Name Meta Event: ',
            0x04: 'Instrument Name Meta Event: ',
            0x05: 'Lyric Meta Event: ',
            0x06: 'Marker Event: ',
            0x07: 'Cue Point Event: ',
            0x7F: 'Proprietary Event: '}
    def parse_file(self):
        """This method is used to start the parsing of the MIDI file.
        It parses the first parts of the file(The MThd chunk and such),
        ]then for each Midi message"""
        self.f.seek(0) #start from the begining
        if self.parse_chunk_header(self.f.read(8))[0] != "MThd":
            #this checks that the first 8 byes in the file are a MThd chunk.
            raise Exception, "Midi files must start with MThd chunk!"
            #if it is, then the program parses it.
        self.Format, self.NumTracks, self.Division\
                     =self.parse_MThd(self.f.read(6))
        #if self.Format != 1:
        #    raise Exception, "Not a Format 1 MIDI file!"
        #and prints out the results...
        self.output("Midi format: "+str(self.Format)+" Number of Tracks: "+str(self.NumTracks))

        #Then, we parse all the rest of the file.
        for x in range(self.NumTracks): #this loop repetes for each track
            
            type, self.chunk_length=self.parse_chunk_header(self.f.read(8)) #parses the header
            self.output(type+" Chunk, with a length of "+ str(self.chunk_length)+" bytes.") #prints it
            begining=self.f.tell() #remembers the start of the track
            
            while self.f.tell() < begining+self.chunk_length: #repetes until the file position is past the end of the track.
                self.parse_MIDI_message()
                
    def parse_VLQ(self): #<-- Variable Length Quanity
        #print "VLQ-> ",
        result=0
        byte=0xFF
        while (byte & 0x80) == 0x80:
            byte=ord(self.f.read(1))
            #print byte,
            result=result << 7
            result=result | (byte & 0x7F)
        #print
        return result
    
    def parse_chunk_header(self, data):
        return struct.unpack(">4sL", data) #returns chunk ID and chunk length
    
    def parse_MThd(self, data):
        return struct.unpack(">HHH", data) #returns Format(0, 1, or 2), NumTracks,
                                       #and Division(which should be processsed
                                       #but I don't know how).
    
    def parse_MIDI_message(self):
        self.output("self.f.tell()="+str(self.f.tell()))
        delta_time=self.parse_VLQ()
        self.output("Delta Time: " + str(delta_time))
        type=ord(self.f.read(1))
        
        if type < 0x80: #running status
            try:
                type=self.last_type
            except NameError:
                raise Execption, "No status byte given for first message!"
            self.f.seek(-1, 1)
        self.last_type=type
        
        if type >= 0x80 and type <= 0xEF:
            if type >> 4 in self.MidiVoiceEvents.keys():
                self.MidiVoiceEvents[type >> 4](type, type & 0xF)
            else:
                self.output('***Unknown Midi Voice Message: '+hex(type)+'***')
        elif type >= 0xF0 and type <= 0xFF:
            if type in self.MidiSystemEvents.keys():
                self.MidiSystemEvents[type](type)
            else:
                self.output('***Unknown Midi System Message: '+hex(type)+'***')
        else:
            raise Exception, 'Incorrect Midi Status byte! :'+hex(type)

    def output(self, text):
        print text

##    def Ignore_Event(self, type, channel=None):
##        while ord(self.f.read(1)) & 0x80 == 0:
##            pass
##        self.f.seek(-1, 1)
##        print ord(self.f.read(1))
##        self.f.seek(-1, 1)
    def NoteOff_VoiceEvent(self, type, channel):
            note=ord(self.f.read(1))
            velocity=ord(self.f.read(1)) #must be zero
            self.output("Note Off Voice Event: Note: "+Nnum2Nname(note)+\
                        " on channel "+str(channel+1))

    def NoteOn_VoiceEvent(self, type, channel):
        note=ord(self.f.read(1))
        velocity=ord(self.f.read(1))
        if velocity==0:
            self.f.seek(-2, 1)
            self.NoteOff_VoiceEvent(type, channel)
        else:
            self.output("Note On Voice Event: Note: "+Nnum2Nname(note)+\
                        " Velocity: "+`velocity`+\
                        " on channel "+`channel+1`)

    def Controller_VoiceEvent(self, type, channel):
        control=ord(self.f.read(1))
        value=ord(self.f.read(1))
        self.output('Controller Voice Event: Controller: '+`control`+\
                    'Value: '+`value`+' on channel '+`channel+1`)

    def PatchChange_VoiceEvent(self, type, channel):
        patch=ord(self.f.read(1))
        self.output("Patch Change Voice Event: Patch: "+str(patch+1)+\
                    " on channel "+str(channel+1))
        
    def ChannelPress_VoiceEvent(self, type, channel):
        self.output("Channel Pressure Voice Event: Pressure: "+\
                    `ord(self.f.read(1))`)
    def PitchWheel_VoiceEvent(self, type, channel):
        value=(ord(self.f.read(1)) << 7) | ord(self.f.read(1))
        self.output("Pitch Wheel Event: Value: "+`value`)

    def Meta_SystemEvent(self, type):
        type2=ord(self.f.read(1))
        length=self.parse_VLQ()
        if type2 in self.MetaEvents.keys():
            self.MetaEvents[type2](type2, length)
        else:
            self.output("***Unknown Meta Event! Status byte: "+ hex(type2)+'***')
            self.f.seek(length, 1)
            
    def seqNum_MetaEvent(self, type, length):
        if length==0:
            self.output('Sequence Number Meta Event: No number given')
        else:
            num=int(`ord(self.f.read(1))`+`ord(self.f.read(1))`)
            self.output('Sequence Number Meta Event: Number: '+`num`)
    def port_MetaEvent(self, type, length):
        self.output('MIDI Port Meta Event: Port: '+`ord(self.f.read(1))`)
    def channel_MetaEvent(self, type, length):
        self.output('MIDI Channel Meta Event: Channel: '+`ord(self.f.read(1))+1`)
    def text_MetaEvent(self, type, length):
        self.output(self.TextTypes[type]+self.f.read(length))

    def endTrk_MetaEvent(self, type, length):
        self.output('End of Track Meta Event!')

    def tempo_MetaEvent(self, type, length):
        tempo=hex2dec(hex(ord(self.f.read(1)))[2:]+\
                      hex(ord(self.f.read(1)))[2:]+\
                      hex(ord(self.f.read(1)))[2:])
        self.output("Tempo Meta Event: "+ str(tempo))
    def SMPTE_MetaEvent(self, type, length):
        SMPTE_info=(ord(self.f.read(1)), ord(self.f.read(1)), ord(self.f.read(1)),\
                    ord(self.f.read(1)), ord(self.f.read(1)))
        self.output('SMPTE Offset Meta Event:'+`SMPTE_info`)
    def timeSig_MetaEvent(self, type, length):
        time_sig=(ord(self.f.read(1)),\
                  2**ord(self.f.read(1)),\
                  ord(self.f.read(1)),\
                  ord(self.f.read(1)))
        self.output("Time Sig Meta Event: "+ str(time_sig))

    def keySig_MetaEvent(self, type, length):
        key_sig=(ord(self.f.read(1)), ord(self.f.read(1)))
        self.output("Key Sig Meta Event: "+ str(key_sig))
        
def parse_file(file_object):
    it=MidiFileObject(file_object)
    it.parse_file()
    return it
def Nnum2Nname(num):
        notes=['C ','Db', 'D ', 'Eb ', 'E', 'F ', 'F#', 'G ', 'G#', 'A ', 'Bb', 'B ']
	return notes[num % 12]+`num/12`

def hex2dec(hex_num):
    if type(hex_num)==types.IntType:
        return hex_num
    if hex_num[:2]=="0x":
        hex_num=hex_num[2:]
    hex_num=string.lower(string.strip(hex_num))
    table={"0":0, "1":1, "2":2, "3":3, "4":4, "5":5, "6":6, "7":7, "8":8, "9":9, "a":10, "b":11, "c":12, "d":13, "e":14, "f":15}
    ans=0
    for q in range(len(hex_num)):
	ans=ans+table[hex_num[q]]*16**(len(hex_num)-(q+1))
    return ans

def oct2dec(oct_num):
    oct_num=str(oct_num)
    ans=0
    for q in range(len(oct_num)):
        if int(oct_num[q]) > 7:
            raise Exception, "Not an octal number!"
        ans=ans+int(oct_num[q])*8**(len(oct_num)-(q+1))
    return ans

def dec2bin(num):
    ans=[]
    for q in range(7, -1, -1):
        ans.append(str(num/2**q % 2))
    return (ans[0]+ans[1]+" "+ans[2]+ans[3]+" "+ans[4]+ans[5]+" "+ans[6]+ans[7])

def bin2dec(bin_num):
    bin_num=string.replace(bin_num, " ", "")
    ans=0
    for q in range(len(bin_num)):
        if int(bin_num[q]) > 1:
            raise Exception, "Not an binary number!"
        ans=ans+int(bin_num[q])*2**(len(bin_num)-(q+1))
    return ans
