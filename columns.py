"""Columns 1.1- A Reimplementation of a Fun Tetris-clone in Python."""

"""Versioning started at version 1.0.  Most of the code was already written.
1.0- First version.
1.1 -Fixed some minor issues with console output.
2.0.1 -- Considerably re-written version based on newer Python and my increased knowledge. (April 2014)
"""

"""TODO:
* tops value inconsistent with visable value.  probably related to misalignment
code.
* add speed control-Done 11:24 PM, 5/25/01
* add automatic speed changing(a la Tetris levels) - Done
* speed calulation problems need to be addressed
* add paren in help text-Done
* fix spelling errors in help text-Done
* reverse direction of height scale in config box-Done
* Add constants setting range for configuration boxes-Done 6/29/01.

"""
from Tkinter import *
import random, time, operator, sys, re, os
#Speed range: 150-400, incr. 10
class Config:
    DATA_FILE='columns.dat'
    PARAMS_TO_SAVE=['width_in_sqrs','height_in_sqrs','numColors','sqr_height','sqr_width',\
                    'FallingSpeed']

    def __init__(self):
        #-----------Defaults--------
        self.width_in_sqrs=5
        self.height_in_sqrs=17
        self.numColors=3
        self.FallingSpeed=200 #in approx. pixels per second
        self.sqr_height=30 #in pixels
        self.sqr_width=30 #in pixels
        #--------End Defaults--------
    
    def handle_data(self, whichWay):
        if not os.access(self.DATA_FILE, os.F_OK):
            f=open(self.DATA_FILE, 'w')
            f.write("[scores]\n"+\
                    r" [(3000, 'Something to take\012a depleted-uraniam\012fully-automatic\012machine-gun\012blast for.'), "+\
                    r"  (1000, 'Something to\012shoot for.'), (234, 'Jesse Weinstein'), (23, 'This is not\012a score.'), (11, 'Visimom')]"+\
                    "\n[parameters]\nsqr_height=20\nnumColors=5\nsqr_width=21\n"+\
                    "FallingSpeed=240\nwidth_in_sqrs=8\nheight_in_sqrs=24\n")
            f.close()

        txt=open(self.DATA_FILE, 'r').read()
        data={}
        for item in re.findall('\\[\\w+\\]\n(?:[^\[].+\n)+', txt):
            data.update({re.search(r'\[\w+\]', item).group(): re.findall('(?<=\n).+', item)})

        for key in data.keys():
            if key[1:-1]+'_file_handler' in dir(self.__class__):
                data[key]=eval('self.'+key[1:-1]+'_file_handler(data[key], "'\
                               +whichWay+'")')
        if whichWay=='save':
            txt=''
            for key, value in data.items():
                txt=txt+''.join([key, '\n']+map(operator.add, value, ['\n']*len(value)))
            f=open(self.DATA_FILE, 'w')
            f.write(txt)
            f.close()
    def scores_file_handler(self, data, whichWay):
#        print data
        if whichWay=='save':
#            print [`self.high_scores`]
            return [' '+`self.high_scores`]
        elif whichWay=='load':
            self.high_scores=eval(data[0])
#            print 'HS: ', self.high_scores
        else:
            raise ValueError, 'Unknown value for whichWay!'
        return data

    def parameters_file_handler(self, data, whichWay):
        splt={}
        for item in data:
            foo=item.split('=')
            splt.update({foo[0]:foo[1]})
        #print 'Parameters file handler:', splt
        if whichWay=='save':
            for param in self.PARAMS_TO_SAVE:
                splt.update({param:eval('`self.'+param+'`')})
            new_data=[]
            for key, value in splt.items():
                new_data.append(key+'='+value)
            return new_data
        elif whichWay=='load':
            for param in self.PARAMS_TO_SAVE:
                exec 'self.'+param+'='+splt.get(param, eval('`self.'+param+'`'))
        else:
            raise ValueError, 'Unknown value for whichWay!'
        return data


class ColumnsGame:
    #to change the range of any of the configuration dialog boxes,
    #change the numbers after the equals sign in the appropriete line below.
    SQR_SIZE_RANGE=[15, 90]
    FALLING_SPEED_RANGE=[150, 400]
    KEY_PRESSES_PER_SPEED_LEVEL=100
    #Play area range and # of Colors range are already nearly as large as they
    #can be, so they are not modifiable here.
    #If you wish to modify the colors of the blocks, see below.
    #This is the end of the configuration ranges.
    
    X_OFFSET=5
    Y_OFFSET=0
    UNIT_DENOMINATOR=4

    MAX_PLAYABLE_TIME=1800

    COLORS=['aquamarine', 'blue', 'red', 'green', 'purple', 'brown', 'green4',\
            'lightblue', 'gold', 'black', 'gray', 'yellow', 'pink', 'blue4',\
            'bisque', 'cornflower blue', 'dark orange', 'violetRed',\
            'yellow green']
    #You may freely reorder these colors, and you may add more if you are sure
    #that Tkinter can recognize them.
    
    SPECIFIED_BLOCKS=[]#2, 2, 0, 2,2,0, 1,1,0, 1,1,0, 2,0,1, 2,0,2, 2,2,1, 1,1,1]    
    HELP_TEXT="""
    This is a re-implementation of a Windows game, also called Columns.

    The object of this game is to rack up as many points as you can before the
blocks reach the top.  You can make blocks disappear by lining them up in threes, horizonitaly,
vertically, or diagonally.  You also get one point for each arrangement of threes possible in your
line(i.e. if you line up four, you get two points, because there are two possible arrangements of
three, and with five, you would get three points).  When blocks disapper, the blocks on top of them
fall, which may cause further lineups and disappearances.

    The keys used are below:
        UP arrow- rotates the order of the blocks; i.e. the bottom block is put on the top, and
            the other two are each dropped one place.
        LEFT and RIGHT arrows- move the stack left or right one column.
        DOWN arrow- causes the block to drop imediately, so you can speed up gameplay.
        
        F2 is the keyboard shortcut for the New Game command.
        F3 or "p" are the keyboard shortcuts for the Pause command.

    That's all, folks!  Have fun playing!"""
    #Constant data structures
    #These are data structures defining possible sets of 3.
    RECT_POINTS=[(-2,0, 0,0), (-1,0, 1,0), (0,0, 2,0),\
                 (0,-2, 0,0), (0,-1, 0,1), (0,0, 0,2)]
    DLG_POINTS=[((-2,-2), (-1,-1), (0,0)), ((-1,-1), (0,0), (1,1)), ((2,2), (1,1), (0,0)),\
                ((2,-2), (1,-1), (0,0)), ((1,-1), (0,0), (-1,1)), ((-2,2), (-1,1), (0,0))]
    TAGS=('top_sqr', 'mdl_sqr', 'btm_sqr')
#-------------------------------------First level methods---------------------------------------------
#                    provide a general overview of the flow of the program    
    def __init__(self):
        self.config=Config()
        self.config.handle_data('load') #load data from file

        #Define variables
        self.StartupTime=time.time()
        self.DoNotInterrupt=0
        self.levelChangeCount = 0
        self.height=self.config.height_in_sqrs*self.config.sqr_height
        self.width=self.config.width_in_sqrs*self.config.sqr_width
        self.last_text=''
        self.keys={}
        self.tops={}
        
        #Create the window, and use apply_configuration to apply all the defaults defined above.
        self.make_window()
        self.apply_configuration()
    
    def make_window(self):
        self.root=Tk()

        self.root.title('Columns- The Greatest Game on Earth')

        self.Paused=IntVar(self.root)
        self.Paused.set(0)
        
        self.root.bind('<Up>', self.set_key)
        self.root.bind('<Left>', self.set_key)
        self.root.bind('<Right>', self.set_key)
        self.root.bind('<Down>', self.set_key)

        self.root.bind('<F2>', self.new_game)

        self.root.bind('p', self.pause)
        self.root.bind('<F3>', self.pause)

        #root.protocol('WM_DELETE_WINDOW', self.quit)
                      
        self.c=Canvas(self.root, width=self.width+self.X_OFFSET,\
                      height=self.height+self.Y_OFFSET, \
                      bg='white', bd=2, relief=RIDGE)
        self.c.pack()
        
        self.score_var=IntVar(self.root)
        self.score_display=Label(self.root, font=('', '16', ''),\
                                 textvariable=self.score_var)
        self.score_display.pack()

        topmenu = MenuWrapper(self.root)
        topmenu.cascade(
            'File') \
            .command('New Game',self.new_game) \
            .command('End Game',self.lose_game) \
            .checkbutton('Pause',self.Paused) \
            .command('High Scores',self.high_score_dlg) \
            .separator() \
            .command('Exit',self.quit)
        topmenu.cascade(
            'Configure') \
            .command('Play area', self.configure_playArea) \
            .command('Square size', self.configure_sqrSize) \
            .command('Falling Speed', self.configure_fallingSpeed) \
            .command('Number of Colors', self.configure_numColors)
        topmenu.command('Help', self.help_dlg)
        topmenu.finish()
    def set_key(self, event):
        self.keys[event.keysym]=True
        
    def pause(self, event=None):
        self.Paused.set(not self.Paused.get())
    def new_game(self, event=None):
        self.c.config(bg='white')
        self.DoNotInterrupt=0
        self.ShouldStop=0
        self.curFallingSpeed=self.config.FallingSpeed
        self.curNumColors=self.config.numColors
        self.curCOLORS=self.COLORS
        self.block_count=0
        
        self.Paused.set(0)
        self.c.delete(ALL)
        self.score_var.set(0)
        for x in range(self.X_OFFSET, self.width+self.X_OFFSET, self.config.sqr_width):
            self.tops.update({float(x+self.config.sqr_width):self.height})
        self.new_block()
        self.move_block()
        
    def new_block(self):
        for tag in self.TAGS:
            self.c.dtag(tag)
        self.c.dtag('cnt_block')

        x=random.randint(0, (self.width/self.config.sqr_width)-1)*self.config.sqr_width\
           +self.X_OFFSET
        y=-(self.curFallingSpeed+self.config.sqr_height*3)

        for count in range(3):
            self.maybe_add_color()
            if self.block_count < len(self.SPECIFIED_BLOCKS):
                col=self.COLORS[self.SPECIFIED_BLOCKS[self.block_count]]
                self.block_count += 1
            else:
                col=random.choice(self.curCOLORS[:self.curNumColors])

            self.c.create_rectangle(x,
                                    y+count*self.config.sqr_height,\
                                    x+self.config.sqr_width,\
                                    y+(count+1)*self.config.sqr_height,\
                                    fill=col,\
                                    tag=(self.TAGS[count], 'cnt_block'))

    def lose_game(self, event=None):
        self.ShouldStop=1
        self.c.config(bg='black')
        self.c.create_text(self.width/2, self.height/2, fill='white', text='Game Over!',\
                           font=('', '15', ''))
        self.check_for_high_score()
#--------------------------------------Second level methods-----------------------------------------------------------
    def move_block(self, event=None):
##        if self.DoNotInterrupt:
##            print 'trying to move block'
        self.maybe_increase_speed()
        if self.ShouldStop:
            return
        if not self.DoNotInterrupt and not self.Paused.get() and self.root.focus_get():
            self.key_pickups()
            for item in self.tops.values(): #check for misalignments
                if item % self.config.sqr_height != 0:
                    raise 'Odd top!', `self.tops`

            #make the blocks fall
            self.c.move('cnt_block', 0, self.curFallingSpeed*0.03)
            
            #process the block if it is fully down:
            crds=self.c.coords('btm_sqr')
##            try:
##                fwqowfw=(crds[3], self.tops[crds[2]])
##            except IndexError:
##                print 'Index Error', crds
            if crds[3] >= self.tops[crds[2]]:
                self.DoNotInterrupt=1
                #self.output('Beginning proccesing')
                #adjust the block to remove misalignment
                if crds[3] != self.tops[crds[2]]:
                    for count in range(3): 
                        obj_crds=self.c.coords(self.TAGS[count])

                        self.c.coords(self.TAGS[count], obj_crds[0], \
                                      self.tops[obj_crds[2]]-(3-count)*self.config.sqr_height,\
                                      obj_crds[2], self.tops[obj_crds[2]]-(2-count)*self.config.sqr_height)
                    crds=self.c.coords('btm_sqr')
                    #self.output(`crds`)
                #print 'Top_sqr', self.c.coords('top_sqr')
                #check for game lost
                if self.c.coords('top_sqr')[1] < 0:
                    self.lose_game()
                    return
                #the top of the column is set to the top of this new block
                self.tops[crds[2]]=self.c.coords('top_sqr')[1]
                #self.output('Tops dict, '+`crds[2]`+' is now '+`self.tops[crds[2]]`)

                lines=self.check_some_squares(self.TAGS)
                if lines:
                    self.do_block_drop(lines)

                self.new_block()
                #print "Done Processing"
                self.DoNotInterrupt=0
        self.root.after(30, self.move_block)

    def move_sideways(self, direction):
        top_crds=self.c.coords('top_sqr')
        btm_crds=self.c.coords('btm_sqr')
        side=top_crds[(direction>0) and 2 or 0]
        new_side=side + direction * self.config.sqr_width/2
        if not self.c.find_overlapping(new_side, top_crds[1], new_side, btm_crds[3]) \
               and ((direction>0 and side < self.width) or
                    (direction<0 and side > self.X_OFFSET)):
            self.levelChangeCount += 1
            self.c.move('cnt_block', direction*self.config.sqr_width, 0)
        
    def key_pickups(self):
        """Process the movement keys."""
        if self.keys.get('Up'): #rotate blocks
            self.levelChangeCount += 1
            foo=[self.c.itemcget(self.TAGS[count], 'fill') for count in [2,0,1]]
            for count in range(3):
                self.c.itemconfig(self.TAGS[count], fill=foo[count])
        if self.keys.get('Left'): #move blocks left
            self.move_sideways(-1)
        if self.keys.get('Right'): #move blocks right
            self.move_sideways(1)
        if self.keys.get('Down'): #drop blocks
            #prevent blocks from being droped without at least the bottom block
            #being fully visible.
            if self.c.coords(self.TAGS[2])[3] > self.config.sqr_height:
                self.levelChangeCount += 1
                for count in range(3):
                    crds=self.c.coords(self.TAGS[count])
                    top=self.tops[crds[2]]
                    self.c.coords(self.TAGS[count], \
                                  crds[0], top-(3-count)*self.config.sqr_height,\
                                  crds[2], top-(2-count)*self.config.sqr_height)
        #Mark all keys as processed
        self.keys={}
        
    def apply_configuration(self):
        self.height=self.config.height_in_sqrs*self.config.sqr_height
        self.width=self.config.width_in_sqrs*self.config.sqr_width
        self.c.config(width=self.width+self.X_OFFSET,\
                      height=self.height+self.Y_OFFSET)
        self.tops={}
        for x in range(self.X_OFFSET, self.width+self.X_OFFSET, self.config.sqr_width):
            self.tops.update({float(x+self.config.sqr_width):self.height})
        self.ShouldStop=1
        self.c.delete(ALL)
        self.score_var.set(0)
#----------------------------------------Helper methods--------------------------------------------------

    def vibrateBlocks(self):
        def inner(n, self):
            if n % 2 ==0:
                self.c.config(bg="black")
            else:
                self.c.config(bg="white")
            if n < 21:
                self.root.after(100, lambda :inner(n+1, self))
        inner(0, self)
    def quit(self, event=None):
        t=time.clock()
        while time.clock()-t < 5 and self.DoNotInterrupt:
            pass
        self.config.handle_data('save')
#        print 'Quitting.'
        self.root.destroy()
    def check_for_high_score(self):
        score=self.score_var.get()
        yes=0
        if len(self.config.high_scores) < 10:
            self.config.high_scores.append((score, self.ask_name()))
            yes=1
        else:
            for item in self.config.high_scores:
                if cmp(score, item[0])==1:
                    self.config.high_scores.append((score, self.ask_name()))
                    yes=1
                    break
            self.config.high_scores.sort()
            self.config.high_scores.reverse()
            del self.config.high_scores[10:]
        if yes:
            self.high_score_dlg()
    def ask_name(self):
        foo=NewHighScoreDlg(self)
        return foo.doIt()#raw_input('Enter your name: ')
    def high_score_dlg(self):
        #Increse font size of scores-Done
        #Add blocks in the background-Done
        old_pause=self.Paused.get()
        self.Paused.set(1)
        self.config.high_scores.sort()
        self.config.high_scores.reverse()
        base=Toplevel(self.root)
        base.transient(self.root)
        base.title('High Scores')
        tot=0
        for item in self.config.high_scores:
            tot += item[1].count('\n')
        c=Canvas(base, bg='white', width=240, height=(len(self.config.high_scores)+tot)*20+60)
        c.pack(pady=5)
        #make blocks

        for UselessVar in range(5):
            while 1:
                x=random.randint(10, 230-self.config.sqr_width)
                y=random.randint(10, (len(self.config.high_scores)+tot)*20+50-(self.config.sqr_height*3))
                if not c.find_overlapping(x, y, x+self.config.sqr_width, y+self.config.sqr_height*3):
                    break
            for count in range(1, 4):
                col=random.choice(self.curCOLORS[:self.config.numColors])
                c.create_rectangle(x, y+(count-1)*self.config.sqr_height,\
                                        x+self.config.sqr_width,\
                                        y+count*self.config.sqr_height,\
                                        fill=col)
        #make score texts
        col=0; num=0
        c.create_text(100, 30, text='High Scores', font=('oklahoma', '16', ''))
        c.create_rectangle(2,2, 240, (len(self.config.high_scores)+tot)*20+60, outline='red', width=2)
        for item in self.config.high_scores:
            num += 1
            col += 1
            c.create_text(15, 30+col*20, anchor=NW, text=`num`, \
                          font=('book antigua', '10', 'bold'))
            c.create_text(40, 30+col*20, anchor=NW, text=item[1], \
                          font=('book antigua', '10', 'bold'))
            c.create_text(190, 30+col*20, anchor=NW, text=`item[0]`, \
                          font=('book antigua', '10', 'bold'))
            col += item[1].count('\n')
        Button(base, text='Done', command=base.destroy).pack()
        self.root.wait_window(base)
        self.Paused.set(old_pause)
    def help_dlg(self):
        old_pause=self.Paused.get()
        self.Paused.set(1)
        base=Toplevel(self.root)
        base.transient(self.root)
        base.title('Help')
        t=Text(base, exportselection=0, font=('', '12', ''))
        t.insert(END, self.HELP_TEXT)
        t.config(state=DISABLED)
        t.grid()
        Button(base, text='Done', command=base.destroy).grid()
        self.root.wait_window(base)
        self.Paused.set(old_pause)
    def configure_fallingSpeed(self):
        it=ConfigureDlg(self.root, \
                        {'label': 'Falling Speed',\
                         'from':self.FALLING_SPEED_RANGE[0], \
                         'to':self.FALLING_SPEED_RANGE[1], 'default':self.config.FallingSpeed, 'resolution':10})
        self.config.FallingSpeed=it.doIt()[0]
        self.apply_configuration()
    def configure_playArea(self):
        it=ConfigureDlg(self.root, \
                        {'label':'Height', 'to':2, \
                         'from':(self.root.winfo_screenheight()*0.84)\
                         /self.config.sqr_height,\
                         'default':self.config.height_in_sqrs}, \
                        {'label':'Width', 'from':3,\
                         'to':(self.root.winfo_screenwidth()*0.9)\
                         /self.config.sqr_height, 'orient':HORIZONTAL,\
                         'default':self.config.width_in_sqrs})
        self.config.height_in_sqrs, self.config.width_in_sqrs=it.doIt()
        self.apply_configuration()
    def configure_sqrSize(self):
        it=ConfigureDlg(self.root, \
                        {'label':'Sqr Height', 'from':self.SQR_SIZE_RANGE[0],\
                         'to':self.SQR_SIZE_RANGE[1], \
                         'default': self.config.sqr_height},\
                        {'label':'Sqr Width', 'from':self.SQR_SIZE_RANGE[0],\
                         'to':self.SQR_SIZE_RANGE[1], \
                         'default': self.config.sqr_width, 'orient':HORIZONTAL},\
                        title='Square Size')
        self.config.sqr_height, self.config.sqr_width=it.doIt()
        self.apply_configuration()
    def configure_numColors(self):
        it=ConfigureDlg(self.root, \
                        {'label':'Number of Colors:',\
                         'from':2,\
                         'to':len(self.COLORS), 'default': self.config.numColors,\
                         'orient':HORIZONTAL}, title='Number of Colors')
        self.config.numColors=it.doIt()[0]
        self.apply_configuration()
    #--------------------------------Details of sqr checking mechanisim----------------------------------------------------------------------
    def delay(self, how_long):
        start=time.clock()
        while time.clock()-start < how_long:
            self.c.update()
        
    def do_block_drop(self, lines):
        if not lines:
            return

        #self.output('block_drop for:'+`lines`)
        if not all(len(line)==3 for line in lines):
            raise 'TooStrangeError', "wrong num of sqr's in this line in block_drop: "+`line`
            
        blocks=set().union(*lines) #flatten

        #columns are described by the middle of blocks(rather than the right
        #edge, as the tops dict does) so that they can be used to
        #find_overlapping blocks without modification.
        columns=set((self.c.coords(sqr)[2]-self.config.sqr_width/2) for sqr in blocks)

        blocks_to_move=[]
        for col in columns:
            #self.output('\nColumn:'+`col`+' '+'%'*15)

            #the bottom edge of the lowest sqr to be removed in the col
            row_max = max(self.c.coords(item)[1] for item in self.c.find_overlapping(col, 0, col, self.height) if item in blocks)
            #self.output('Row Max: '+`row_max`)

            #all the sqrs above the row_max.
            boxes_above=self.c.find_overlapping(col, 0, col, row_max)
            
            #for each item above row_max which is not to be removed, enter
            #it in blocks_to_move, along with the distence to move it.
            #the distence is figured by multiplying sqr_height by the number of
            #sqrs to be removed below the item
            def num_below(item):
                return sum(1 for item2 in self.c.find_overlapping(
                    col, self.c.coords(item)[3], col, self.height) if item2 in blocks)
            
            blocks_to_move += [(item, self.config.sqr_height*num_below(item))
                               for item in boxes_above if item not in blocks]

            #adjust the tops entry for this column by the number of blocks to be
            #removed.
            num_blocks_to_remove=sum(1 for item in boxes_above if item in blocks)

            self.tops[col+self.config.sqr_width/2] += (num_blocks_to_remove*self.config.sqr_height)
            #self.output('Tops dict, '+`col+self.config.sqr_width/2`+' is now '+`self.tops[col+self.config.sqr_width/2]`)

        #Actually do it...
        self.delay(0.1)
        #tag the blocks
        for sqr in blocks: 
            self.c.itemconfig(sqr, tag='block_drop_temp_tag')
        self.c.itemconfig('block_drop_temp_tag', fill='white') #flash the blocks to be removed
        self.delay(0.2)
        self.c.delete('block_drop_temp_tag')
        self.c.itemconfig(ALL, width=1)
        
        #set the new score
        self.score_var.set(self.score_var.get()+len(lines))

        #move the blocks that should be moved
        #self.output('blocks to move:'+`blocks_to_move`)
        for (item, how_far) in blocks_to_move:
            self.c.move(item, 0, how_far)

        #Repeat, if needed...
        self.do_block_drop(self.check_some_squares([item for (item, how_far) in blocks_to_move]))

    def check_some_squares(self, squares):
        """Returns a set of three-tuples of Tk object id's."""
        return set().union(*[self.check_a_sqr(self.c.coords(item))
                             for item in squares])

    def check_a_sqr(self, pos):
        #self.output('chk')
        if len(pos) != 4:
            raise 'Exception', 'wrong args for check_a_sqr: '+`pos`

        lines=[] #this holds the found lines
        def is_it_a_line(line):
            """Highlight and add to list if given exactly 3 Tk item IDs with the same fill color."""
            if len(line)==3 and len(set(self.c.itemconfig(item, 'fill') for item in line))==1:
                for item in line: #bold the outline
                    self.c.lift(item)
                    self.c.itemconfig(item, width=4)
                lines.append(line)

        ctr=((pos[2]+pos[0])/2, (pos[3]+pos[1])/2) #the center point
        def convert(pt): return ( \
            (pt[0]*self.config.sqr_width)+ctr[0],\
            (pt[1]*self.config.sqr_height)+ctr[1])

        #The Orthogonal Line Checker
        for pair_of_points in self.RECT_POINTS:
            crds=convert(pair_of_points[:2])+convert(pair_of_points[-2:])

            #self.c.create_rectangle(outline='black', width=2, tag='hi', *crds)
            #self.delay(0.3)
            #self.c.delete('hi')

            is_it_a_line(self.c.find_overlapping(*crds))

            
        #The Diagonal Line Checker
        for set_of_points in self.DLG_POINTS:
            #for point in set_of_points:
            #    self.c.create_rectangle(outline='black', width=2, tag='hi', *(convert(point)*2))
            #self.delay(0.3)
            #self.c.delete('hi')
            
            is_it_a_line(tuple([sqr_tuple[0] for sqr_tuple in
                          [self.c.find_overlapping(*(convert(point)*2))
                           for point in set_of_points] if len(sqr_tuple)==1]))

        if lines: print lines
        
        return lines

    # ------------------------More Helper Methods------------------------------------
    def maybe_increase_speed(self):
        if self.levelChangeCount > self.KEY_PRESSES_PER_SPEED_LEVEL:
            self.levelChangeCount = 0
            self.curFallingSpeed += (self.FALLING_SPEED_RANGE[1]-self.FALLING_SPEED_RANGE[0])/10
            self.vibrateBlocks()

    def maybe_add_color(self):
        dur=time.time() - self.StartupTime #seconds
        if dur > 5 and (dur > self.MAX_PLAYABLE_TIME or random.randint(int(dur), self.MAX_PLAYABLE_TIME) == 0):
            col="#%02x%02x%02x" % (random.randint(0, 255),\
                                   random.randint(0, 255),\
                                   random.randint(0, 255))
            #print col
            self.curCOLORS.insert(1, col)
            self.curNumColors += 1

    def output(self, text):
        #return
        if self.last_text != text:
            print '\n'+text,
            self.last_text=text
        else:
            print '* ',

            
class ConfigureDlg:
    def __init__(self, master, *config_items, **root_options):
        self.master=master
        self.config_items=config_items
        self.root_options=root_options
        self.title='Configuration'
        self.defaults=[]
        self.done=0
        if 'title' in self.root_options.keys():
            self.title=self.root_options['title']
            del self.root_options['title']
        for item in self.config_items:
            if 'default' in item.keys():
                self.defaults.append(item['default'])
                del item['default']
            else:
                self.defaults.append(item['from'])
    def doIt(self):
        root=Toplevel(self.master, **self.root_options)
        root.transient(self.master)
        root.title(self.title)
        root.protocol('WM_DELETE_WINDOW', self.cancel_callback)
        count=0
        for item in self.config_items:
            item.update({'widget':Scale(root, **item)})
            item['widget'].set(self.defaults[count])
            item['widget'].grid()
            count += 1
        Button(root, text='Ok', command=self.ok_callback).grid()
        Button(root, text='Cancel', command=self.cancel_callback).grid()
        while not self.done:
            root.update()
        answer=[]
        for item in self.config_items:
            answer.append(item['widget'].get())
        root.destroy()
        return answer
    def cancel_callback(self):
        count=0
        for item in self.config_items:
            item['widget'].set(self.defaults[count])
            count += 1 
        self.done=1
    def ok_callback(self):
        self.done=1
        
class NewHighScoreDlg:
    def __init__(self, app):
        self.app=app
    def finished(self, event):
            foo=self.entry.get()
            if not foo:
                    return None
            else:
                    
                    self.ans
                    self.ans=foo
    def doIt(self):
        root=Tk()
        c=Canvas(root, width=330, height=110, bg='white')
        c.pack()
        root.title('A High Score!')
        c.create_text(10, 10, text='You made a', font=('', '16', ''), anchor=NW)
        text='High Score!'
        for count in range(len(text)):
                c.create_text(165+count*15, 12, anchor=NW, text=text[count], font=('Courier New', '16', 'bold'), fill=self.app.COLORS[count % self.app.config.numColors])
        self.entry=Entry(root)
        self.entry.bind('<Return>', self.finished)
        self.ans=''
        c.create_text(10, 70, anchor=NW, text='Enter your name:\n(as you want it to appear on the high score list)')
        c.create_window(120, 63, anchor=NW, window=self.entry)
        while not self.ans:
                root.update()
        root.destroy()
        return self.ans

class MenuWrapper:
    """Wrapper around Tkinter.Menu to provide a nicer interface."""
    def __init__(self, parent):
        self.parent=parent
        self.m=Menu(parent,tearoff=0)
    def command(self, label, cmd):
        self.m.add_command(label=label, command=cmd)
        return self
    def separator(self):
        self.m.add_separator()
        return self
    def checkbutton(self, label, var):
        self.m.add_checkbutton(label=label, variable=var)
        return self
    def cascade(self, label):
        mw=MenuWrapper(self.m)
        self.m.add_cascade(label=label, menu=mw.m)
        return mw
    def finish(self):
        self.parent.config(menu=self.m)

def IDLEtest():
    """Returns 1 when IDLE is running, 0 else.
    Please let me know (through the IDLE-devl list) if you find a situation
    where this gives the wrong answer."""
    import sys
    try:
        if sys.stdin.__module__=='PyShell':
            return 1
        else:
            return 0
    except AttributeError:
        return 0

if __name__=='__main__':
    #if you want to recreate a previous game, insert the random seed for it into
    #the appropriete command below.
    random_seed=long(time.time() * 256)
    print 'random_seed =', random_seed
    random.seed(random_seed)
    
    x=ColumnsGame()
    if not IDLEtest():
        x.root.mainloop()
