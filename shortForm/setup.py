"""Short Form prevents the Python shell from printing out giant piles of text.

It is a hack that ties into the display system. When you are working with a
multi-megabyte text file, referenced under the name `txt`, which takes 10
minutes to be printed in full (if you accidentally type '>>> txt') it's
really nice to have this.

Besides actually turning it off, it can be evaded by using `print` or
even sys.stdout.write itself.
ShortForm is only used when an object is repr'ed and printed."""

import sys

classifiers = """\
Development Status :: 4 - Beta
Intended Audience :: Developers
License :: OSI Approved :: MIT License
Natural Language :: English
Operating System :: OS Independent
Programming Language :: Python
Topic :: Software Development
Topic :: Software Development :: Interpreters
Topic :: Software Development :: Pre-processors
Topic :: Text Processing :: Filters
Topic :: Utilities
"""

doc=__doc__.split("\n")
# patch distutils if it can't cope with the "classifiers" or
# "download_url" keywords
if sys.version < '2.2.3':
    from distutils.dist import DistributionMetadata
    DistributionMetadata.classifiers = None
    DistributionMetadata.download_url = None
from distutils.core import setup
setup(name='shortForm',
      version='1.1',
      author="Jesse Weinstein",
      author_email="http://purl.org/NET/JesseW/email@purl.org",
      maintainer="Jesse Weinstein",
      maintainer_email="jessw@netwood.net",
      url="http://purl.oclc.org/NET/JesseW/Python",
      download_url="http://purl.oclc.org/NET/JesseW/SundryStuff/shortForm-1.1.zip",
      license="http://www.opensource.org/licenses/mit-license.html",
      description=doc[0],
      long_description="\n".join(doc[2:]),
      classifiers = filter(None, classifiers.split("\n")),
      py_modules=['shortForm']
      )
