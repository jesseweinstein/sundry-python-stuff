#My version of Yachtz. (based on a program by Timothy M. Brauch)
#Jesse Weinstein, Augest 17, 2001
import random, re, string, operator

class Yacht:
    NUM_TURNS=13
    def __init__(self): #eventually, allow for more than one player
        self.dice=[0]*5
        self.score_types={'upper':('Ones', 'Twos', 'Threes', 'Fours', 'Fives', 'Sixes'), \
                          'lower':('3 of a kind', '4 of a kind', 'Full House',\
                                   'Sm Straight', 'Lg Straight', 'Yahtzee', 'Chance')}
        self.score={'upper':[0]*len(self.score_types['upper']),\
                    'lower':[0]*len(self.score_types['lower'])}
        self.top_bonus=0
        self.dirty=0
        self.saved=[]
        self.scored=[]
        for worthless_var in range(0, self.NUM_TURNS):
            self.turn()
        self.want_to_quit()

    def roll(self):
        print self.saved
        for count in range(5):
            if count not in self.saved:
                self.dice[count]=random.randint(1,6)
        self.dice_display()

    def dice_display(self):
        print '\n'+\
              'Die #     1  2  3  4  5\n'+\
              'Roll '+`self.rolls+1`+'    %i  %i  %i  %i  %i' % tuple(self.dice)
        self.rolls+=1
        self.dirty=0

    def saver(self):
        self.saved=[]
        if self.rolls==3:
            return
        while 1:
            odd_parts=[]
            parts=re.split('[ ,]', raw_input('\nKeep which dice? '))
            if parts==['']:
                return
            print 'parts', parts
            for part in parts[:]:
                if part=='':
                    parts.remove(part)
                    continue
                if part not in ['1', '2', '3', '4', '5']: #i.e. if it is not a reasonable result.
                    odd_parts.append(part)
            if odd_parts:
#                print 'I do not understand these parts of your answer: '+`odd_parts`
                print 'What?'
            else:
                break
        #parts gotten from the time through the loop when there were no odd_parts
        for part in parts:
            self.saved.append(int(part)-1)

    def turn(self):
        self.rolls=0
        self.saved=[]
        while self.rolls<3:
            self.roll()
            self.saver()
        print '\n\nYour turn is over, you must pick a scoring section\n'
        self.score_display()
        self.score_choice()

#Begin score options
    def score_display(self): # Need to fix if score in double digits
##        print 'A) Ones ====> '+str(self.score['ones'])+'      G) 3 of a kind ====> '+str(self.score['3_kind'])
##        print 'B) Twos ====> '+str(self.score['twos'])+'      H) 4 of a kind ====> '+str(self.score['4_kind'])
##        print 'C) Threes ==> '+str(self.score['threes'])+'      I) Full House =====> '+str(self.score['full'])
##        print 'D) Fours ===> '+str(self.score['fours'])+'      J) Sm Straight ====> '+str(self.score['sm_str'])
##        print 'E) Fives ===> '+str(self.score['fives'])+'      K) Lg Straight ====> '+str(self.score['lg_str'])
##        print 'F) Sixes ===> '+str(self.score['sixes'])+'      L) Yahtzee ========> '+str(self.score['Y'])
##        print 'TopBonus ===> '+str(self.score['top_bonus'])+'      M) Chance =========> '+str(self.score['chance'])
##        print '**    TOTAL ===> '+self.total()
        #The non-written out version of the above.  hahahahahahah.
        dis=[]
        for count in range(len(self.score_types['upper'])):
            up=self.score_types['upper'][count]
            up_score=`self.score['upper'][count]`
            lo=self.score_types['lower'][count]
            
            dis.append(string.uppercase[count]+') '+up+' '+'='*(8-len(up))+'> '+\
                       up_score+'     '+' '*(len(up_score)==1)+\
                       string.uppercase[count+len(self.score['upper'])]+') '+lo+' '+'='*(15-len(lo))+'> '+\
                       `self.score['lower'][count]`)
        lo=self.score_types['lower'][count+1]
        dis.append('TopBonus ===> '+`self.top_bonus`+'      '+\
                       string.uppercase[1+count+len(self.score['upper'])]+') '+\
                       lo+' '+'='*(15-len(lo))+'> '+\
                       `self.score['lower'][count+1]`)
        dis.append('**    TOTAL ===> '+`self.total()`)
        print '\n'.join(dis)
    def score_choice(self):
        while 1:
            option=raw_input('Please choose [A-M] ').upper()
            if len(option) == 1 and option in string.uppercase[:len(self.score_types['upper'])+len(self.score_types['lower'])+1]:
                num=string.uppercase.index(option)
                if num not in self.scored:
                    if num==11 and self.dirty==1:
                        print "Haven't you already claimed this Yahtzee?"
                        print 'Please choose a *different* scoring category.'
                    else:
                        break
                else:
                    print 'Please choose another, unused category.'
            else:
                print 'Please choose a valid letter.'
        #sT is scoreType
        print num #debug
        print ''

        if num !=11:
            self.scored.append(num)
            
        self.num=num
        if num<len(self.score_types['upper']):
            self.score_single()
        elif num<=7:
            self.score_kind()
        elif num==8:
            self.score_full_house()
        elif num==9:
            self.score_sm_str()
        elif num==10:
            self.score_lg_str()
        elif num==11:
            self.score_yacht()
        elif num==12:
            self.score_chance()
        else:
            #this should not happen.
            raise Error, 'Not happen clause raised in score_choice!'
##        if option not in self.scored:
##            elif option in 'Hh':
##                self.score_kind(std=4, cat='4_kind')
##                self.scored.append('H')
##                self.scored.append('h')
##            elif option in 'Ii':
##                self.score_full_house()
##                self.scored.append('I')
##                self.scored.append('i')
##            elif option in 'Jj':
##                self.score_sm_str()
##                self.scored.append('J')
##                self.scored.append('j')
##            elif option in 'Kk':
##                self.score_lg_str()
##                self.scored.append('K')
##                self.scored.append('k')
##            elif option in 'Ll':
##                self.score_yacht()
##            elif option in 'Mm':
##                self.score_chance()
##                self.scored.append('M')
##                self.scored.append('m')
##            else:
##                pass
##        else:
##            pass
        if self.top_bonus==0 and reduce(operator.add, self.score['upper']) >= 63:
            self.top_bonus=35
        self.score_display()

# Begin scoring calculations
    def score_single(self):
#        print num
#        print self.dice.count(num+1)
        self.score['upper'][self.num]=self.dice.count(self.num+1)*(self.num+1)
            
    def score_kind(self):
        cnt=0
        for val in self.dice:
            if self.dice.count(val)>cnt:
                cnt=self.dice.count(val)
        if (self.num==6 and cnt>=3) or (self.num==7 and cnt>=4):
            self.score['lower'][self.num-6]=reduce(operator.add, self.dice)
    def score_full_house(self):
        data=self.dice[:]
        data.sort()
#        print 'data', data
        cnt1, cnt2=data.count(data[0]), data.count(data[-1])
        if cnt1==2 and cnt2==3:
            self.score['lower'][self.num-6]=25
        elif cnt1==3 and cnt2==2:
            self.score['lower'][self.num-6]=25
        elif cnt1==5 and cnt2==5:
            self.score['lower'][self.num-6]=25

    def score_sm_str(self):
        if 3 not in self.dice or 4 not in self.dice:
            return
        if 2 in self.dice:
            if 1 in self.dice or 5 in self.dice:
                self.score['lower'][self.num-6]=30
        elif 5 in self.dice and 6 in self.dice:
            self.score['lower'][self.num-6]=30

    def score_lg_str(self):
        data=self.dice[:]
        data.sort()
        if data==[1, 2, 3, 4, 5] or data==[2, 3, 4, 5, 6]:
            self.score['lower'][self.num-6]=40

    def score_yacht(self):
        if self.dice.count(self.dice[0])==5:
            if self.score['lower'][self.num-6]==0:
                self.score['lower'][self.num-6]=50
            elif self.score['lower'][self.num-6]>=50 and self.dirty==0:
                print 'Another Yahtzee?  Good job.'
                dummy=raw_input('Press enter to record this Yahtzee and choose another scoring category.')
                self.dirty=1
                self.score['lower'][self.num-6]+=100
                self.score_display()
                self.score_choice()
        else:
            self.scored.append(num)
    def score_chance(self):
        self.score['lower'][self.num-6]=reduce(operator.add, self.dice)

    def total(self):
        return reduce(operator.add, self.score['upper']+self.score['lower'])

    def want_to_quit(self):
        while 1:
            dummy=raw_input('Play again? [Y/N] ').lower()[0]
        if dummy=='n':
            return 1
        elif dummy=='y':
            return 0
Yacht()
