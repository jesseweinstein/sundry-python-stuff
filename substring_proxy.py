#!/usr/bin/python3
"""Stupidly trivial Flask app that returns a substring of the contents of a URL.

Released into the public domain."""
from urllib.request import urlopen
from flask import Flask
import lxml.html
import lxml.cssselect
import lxml.etree

app = Flask(__name__)


def stringify_children(node):
    # Copied from https://stackoverflow.com/a/24151860/450246
    s = node.text
    if s is None:
        s = ''
    for child in node:
        s += lxml.etree.tostring(child, encoding='unicode', method='html')
    return s


@app.route('/selector/<selector>/url/<path:url>')
def selector_proxy(selector, url):
    with urlopen(url) as f:
        tree = lxml.html.parse(f)
        sel = lxml.cssselect.CSSSelector(selector)
        return stringify_children(sel(tree)[0])


@app.route('/mark/<int:every>/url/<path:url>')
def mark_offsets(every, url):
    with urlopen(url) as f:
        resp = []
        n = 0
        while (n % every) == 0:
            txt = f.read(every)
            n += len(txt)
            resp.append(txt)
            resp.append(('!!%d!!' % n).encode('utf-8'))

        return b''.join(resp)


@app.route('/substring/<int:start>/<int:length>/url/<path:url>')
def substring_proxy(start, length, url):
    with urlopen(url) as f:
        f.read(int(start))
        return (f.read(int(length)),
                f.code, f.headers.raw_items())


@app.route('/')
@app.route('/help')
def help():
    return """<html><head><title>Substring & CSS Selector Proxy</title></head><body>
    <h1>Substring & CSS Selector Proxy</h1>
    <p>This is a tiny web service that merely provides a way to extract parts of other
    URL-accessible content and refer to these extractions with their own URLs.</p>

    <p>It has 3 modes. <code>/substring</code>, <code>/selector</code> and <code>/mark</code>.</p>

    <p>Each of them is accessed with URLs of the form:
    <code><b>/</b>&lt;mode><b>/</b>&lt;args><b>/url/</b>&lt;url></code>,
    where <code>&lt;mode></code> is one of the modes mentioned above, <code>&lt;args></code> are
    one or more arguments to the mode (separated by slashes), and <code>&lt;url></code> is
    the URL whose content will be extracted.</p>

    <p><code>/substring</code> takes two arguments, start and length, and returns the substring
    specified by them.<br/>
    <code>/selector</code> takes one argument, a CSS selector (interpreted by
    <a href="http://lxml.de/cssselect.html">lxml.cssselect</a>) and returns the inner HTML of
    the first element matched by it.<br/>
    <code>/mark</code> is a (not very) useful tool for figuring out start and length values.
    It takes one arguemnt specifying how many characters between each marker, and repeatedly
    inserts the file offset every that many characters. Improvements would be welcomed.
    </p>

    <p>There is no error checking currently, sorry.</p>
    </body></html>"""
if __name__ == '__main__':
    app.run(debug=True)
