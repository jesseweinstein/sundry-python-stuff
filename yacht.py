#Edited by Jesse W 
#Sections of code or coments I added begin and end with a comment like this: #JW
#Sections of code or coments I removed begin and end with a comment like this: #Cut by JW

#There is some stuff I can just change in bits.  But I also have some big changes.  I will
#do them in a sepearate file.
import random,re, string

class Yacht:
    def __init__(self): #eventually, allow for more than one player
        self.dice={'die_1':0, 'die_2':0, 'die_3':0, 'die_4':0, 'die_5':0}
##        self.score={'ones':0, 'twos':0, 'threes':0, 'fours':0, 'fives':0, 'sixes':0, #Cut by JW
##                    '3_kind':0, '4_kind':0, 'full':0, 'sm_str':0, 'lg_str':0,
##                    'top_bonus':0, 'chance':0, 'Y':0} #Cut by JW
        self.score={'ones':[0,0], 'twos':[0,1], 'threes':[0,2], 'fours':[0,3],\
                    'fives':[0,4], 'sixes':[0,5],
                    '3_kind':[0,6], '4_kind':[0,7], 'full':[0,8], 'sm_str':[0,9],\
                    'lg_str':[0,10], 'top_bonus':[0,11], 'chance':[0,12], 'Y':[0,13]}
        #each value is a tuple of the score, and the order number.
        #Note: It is good to keep lines to 80 characters.  However, I don't do it all the time, either.
        self.dirty=0
        self.saved=[]
        self.turns_taken=0
        self.scored=[]
        while (self.turns_taken<13):
            self.turn()
            self.turns_taken+=1
        #this could be done as a for loop, thereby removing the self.turns_taken var. #JW
        #also, turns_taken does not need to be a instence var becasue it is only used in
        #this method. #JW
        self.game_over()

    def roll(self,n):
        for die in self.dice.keys():
            if die not in self.saved:
                self.dice[die]=random.randint(1,6)
        self.dice_display(n)

    def dice_display(self,n):
        print '\n'
        print 'Die #     1  2  3  4  5 '
        print 'Roll '+str(n)+'   ',
        for d in xrange(1,6): print str(self.dice['die_'+str(d)])+' ',
        #you don't need xrange here!  5 items is a _small_ list. #JW
        self.rolls+=1
        self.dirty=0

    def saver(self):
##        self.saved=[] #Cut by JW
##        question=raw_input('\nKeep which dice? ')
##        for num in eval(question):
##            self.saved.append('die_'+str(num)) #Cut by JW
        #you need more error checking here. #JW
        #for example, you can't save less than two dice without an exception being thrown! 
        #Here is a better, although not even close to perfect, form:
        self.saved=[]
        while 1:
            odd_parts=[]
            parts=re.split('[ ,]', raw_input('\nKeep which dice? '))
            for part in parts:
                if part not in ['1', '2', '3', '4', '5']: #i.e. if it is not a reasonable result.
                    odd_parts.append(part)
            if odd_parts:
                print 'I do not understand these parts of your answer: '+odd_parts
                print 'So I will ask you again.'
            else:
                break
        #parts gotten from the time through the loop when there were no odd_parts
        for part in parts:
            self.saved.append('die_'+str(num))
    def turn(self):
        #I don't know why you had this n argument.  self.rolls has exactly the same info in it
        self.rolls=0
        while self.rolls<3:
            while self.rolls<2:
                self.roll(n=(self.rolls+1))
                self.saver()
            self.roll(n=(self.rolls+1))
        print '\n\nYour turn is over, you must pick a scoring section\n'
        self.saved=[] #why is this not at the top of this method?
        self.score_display()
        self.score_choice()

#Begin score options
    def score_display(self): # Need to fix if score in double digits
        #Cut by JW
##        print 'A) Ones ====> '+str(self.score['ones'])+'      G) 3 of a kind ====> '+str(self.score['3_kind'])
##        print 'B) Twos ====> '+str(self.score['twos'])+'      H) 4 of a kind ====> '+str(self.score['4_kind'])
##        print 'C) Threes ==> '+str(self.score['threes'])+'      I) Full House =====> '+str(self.score['full'])
##        print 'D) Fours ===> '+str(self.score['fours'])+'      J) Sm Straight ====> '+str(self.score['sm_str'])
##        print 'E) Fives ===> '+str(self.score['fives'])+'      K) Lg Straight ====> '+str(self.score['lg_str'])
##        print 'F) Sixes ===> '+str(self.score['sixes'])+'      L) Yahtzee ========> '+str(self.score['Y'])
##        print 'TopBonus ===> '+str(self.score['top_bonus'])+'      M) Chance =========> '+str(self.score['chance'])
##        print '**    TOTAL ===> '+self.total()
        #Cut by JW
        #Fun, fun, fun. Since you are using a dictionary for holding the scores(which is a #JW
        #good idea, by the way), there is a much cuter way of doing this.  Drum-roll, please.
        #It does require one modification in self.score, however.  See above.
        items=self.scores.items() #returns a list of keys and value pairs.
        dis=[] #dis for display. It's a list to make adding items faster.
        for count in range(len(self.score)):
            dis.append(
    def score_choice(self):
        option=raw_input('Please choose [A-M] ')
        print ''
        if option not in self.scored:
            if option in 'Aa':
                self.score_single(val=1,cat='ones')
                self.scored.append('A')
                self.scored.append('a')
            elif option in 'Bb':
                self.score_single(val=2,cat='twos')
                self.scored.append('B')
                self.scored.append('b')
            elif option in 'Cc':
                self.score_single(val=3,cat='threes')
                self.scored.append('C')
                self.scored.append('c')
            elif option in 'Dd':
                self.score_single(val=4,cat='fours')
                self.scored.append('D')
                self.scored.append('d')
            elif option in 'Ee':
                self.score_single(val=5,cat='fives')
                self.scored.append('E')
                self.scored.append('e')
            elif option in 'Ff':
                self.score_single(val=6,cat='sixes')
                self.scored.append('F')
                self.scored.append('f')
            elif option in 'Gg':
                self.score_kind(std=3, cat='3_kind')
                self.scored.append('G')
                self.scored.append('g')
            elif option in 'Hh':
                self.score_kind(std=4, cat='4_kind')
                self.scored.append('H')
                self.scored.append('h')
            elif option in 'Ii':
                self.score_full_house()
                self.scored.append('I')
                self.scored.append('i')
            elif option in 'Jj':
                self.score_sm_str()
                self.scored.append('J')
                self.scored.append('j')
            elif option in 'Kk':
                self.score_lg_str()
                self.scored.append('K')
                self.scored.append('k')
            elif option in 'Ll':
                self.score_yacht()
            elif option in 'Mm':
                self.score_chance()
                self.scored.append('M')
                self.scored.append('m')
            else:
                print 'Please choose a valid letter.'
                self.score_choice()
        else:
            print 'Please choose another, unused category.'
            self.score_choice()
        self.score_top_bonus()
        self.score_display()

# Begin scoring calculations
    def score_single(self,val,cat):
        sum=0
        for die in self.dice.keys():
            if self.dice[die]==val: sum=sum+val
            else:pass
##        self.score[cat]=sum #Cut by JW
        self.score[cat][0]=sum # [0] added because each score is now a tuple and you only
        #want to set the first one. #JW

    def score_top_bonus(self):
##        sum=(self.score['ones']+self.score['twos']+self.score['threes']+self.score['fours']+self.score['fives']+self.score['sixes']) #Cut by JW
        sum=(self.score['ones'][0]+self.score['twos'][0]+self.score['threes'][0]+\
             self.score['fours'][0]+self.score['fives'][0]+self.score['sixes'][0]) #JW
        #same as above. #JW
        if sum >= 63:
            self.score['top_bonus'][0]=35 #same as above.
        else: pass #you don't need this else clause. #JW

    def score_kind(self,std,cat):
        cnt=0
        for val in self.dice.values():
            if self.dice.values().count(val)>cnt:
                cnt=self.dice.values().count(val)
            else:pass
        if cnt>=std:
            self.score[cat][0]=self.sum()
        else:pass

    def score_full_house(self):
        data=self.dice.values()
        data.sort()
        cnt1, cnt2=data.count(data[0]), data.count(data[-1])
        if cnt1==2 and cnt2==3:
            self.score['full'][0]=25
        elif cnt1==3 and cnt2==2:
            self.score['full']=25
        elif cnt1==5 and cnt2==5:
            self.score['full']=25
        else:pass

    def score_sm_str(self):
        data=self.dice.values()
        data.sort()
        if 1 and 2 and 3 and 4 in data:
            self.score['sm_str']=30
        elif 2 and 3 and 4 and 5 in data:
            self.score['sm_str']=30
        elif 3 and 4 and 5 and 6 in data:
            self.score['sm_str']=30
        else:pass

    def score_lg_str(self):
        data=self.dice.values()
        data.sort()
        if data==[1, 2, 3, 4, 5]:
            self.score['lg_str']=40
        elif data==[2, 3, 4, 5, 6]:
            self.score['lg_str']=40
        else: pass

    def score_yacht(self):
        if self.dice.values().count(self.dice.values()[0])==5:
            if self.score['Y']==0:
                self.score['Y']=50
            elif self.score['Y']>=50 and self.dirty==0:
                print 'Another Yahtzee?  Good job.'
                dummy=raw_input('Press enter to record this Yahtzee and choose another scoring category.')
                self.dirty=1
                self.score['Y']=self.score['Y']+100
                self.score_display()
                self.score_choice()
            elif self.score['Y']>=50 and self.dirty==1:
                print "Haven't you already claimed this Yahtzee?"
                print 'Please choose a *different* scoring category.'
                self.score_choice()
            else: pass
        else:
            self.scored.append('L')
            self.scored.append('l')

    def score_chance(self):
        self.score['chance']=self.sum()

    def total(self):
        sum=0
        for pt in self.score.keys():
            sum=sum+self.score[pt]
        return str(sum)

    def sum(self):
        sum=0
        for val in self.dice.keys():
            sum=sum+self.dice[val]
        return sum

    def game_over(self):
        dummy=raw_input('Play again? [Y/N] ')
        if dummy in 'Yy': Yacht()
        elif dummy in 'Nn':
            import sys
            sys.exit()
        else:
            self.game_over()

Yacht()
