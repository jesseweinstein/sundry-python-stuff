"""

                The Babble Generator- For all your content-free needs!

The babble module consists of a RandomPhraseMaker class and a subclass of that,
called StarTrekBabbleGenerator.  The RandomPhraseMaker offers a simple way to
make any sort of babble you wish.  Just provide the necessary word lists in a
subclass, or by instanting the RandomPhraseMaker and providing them then.
Once you have an instence with the wordlists installed, just call the instence
to supply all your babbling needs.

                                        Released by Jesse Weinstein
                                        jessw@loop.com
                                        on Feb 20, 2001
"""
import random, types

class RandomPhraseMaker:

    def __init__(self, Wordlists=None):
        if Wordlists != None:
            if type(Wordlists) != types.ListType:
                raise 'TypeError', '"Wordlists" is not a list!'
            if len(Wordlists) == 0:
                raise 'TypeError', '"Wordlists" is empty!'
            if type(Wordlists[0]) != types.ListType:
                raise 'TypeError', '"Wordlists" is not a list of lists!"'
            self.words=Wordlists
        else: 
            self.assign_wordlists()

    def assign_wordlists(self):
        """In a subclass, this would be filled with lists of words.  Here is
        just an example."""
        self.words=[\
            ['one ', 'two ', 'three ', 'four '],\
            ['blue', 'yellow', 'green']\
            ]
        raise 'Exception',\
              'No wordlists given, and no assign_wordlists function!'
    
    def make_phrase(self):
        word = ""
        for wordlist in self.words:
            word = word + random.choice(wordlist)
        return word
    
    def __call__(self):
        return self.make_phrase()
        
class StarTrekBabbleGenerator(RandomPhraseMaker):

    def assign_wordlists(self):
        
        self.words=[\
            ["","Ambient ", "Annular ", "Quantum ",\
             "Spatial ", "Trans-"],\
            ["anahasic ", "axionic ", "biomimetic ", \
             "cosmic ", "duodynetic ", "dynamic ", \
             "genetic ", "interphasic ", "isomiatic ", \
             "magnascopic ", "metagenic ", "metaphasic ", \
             "nueucleonic ", "osmotic ", "plasmonic ", \
             "positronic ", "static ", "subharmonic "],\
            ["", "confinement ", "containment ",\
             "energy ", "flux ", "impulse ", \
             "particle ", "plasma ", "vertion ",\
             "warp "],\
            ["", "beam ", "being ", "bubble ", "burst ",\
             "capacitor ", "conduit ", "converter ", "core ", \
             "discriminator ", "field ", "scanner ", "sensor ", \
             "shell ", "stream ", "string "]\
            ]
if __name__=='__main__':        
    STBG=StarTrekBabbleGenerator()
    for tmp in range(10):
        STBG.make_phrase()
        
