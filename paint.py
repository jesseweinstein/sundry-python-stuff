""""Modified (and hopefuly enhanced) by Jesse Weinstein on Feb 19, 2001 and
before.

Changes:
  * I put it in a class.
  * I created a system of modes(such as free drawing, circles, rectangles), to
  allow easier creation and matainence.
  * I added color support;
  * and line width modification.
Adding most other modes(rectangles, polygons, etc.) and attribute
modifications(fill color, stipple, etc.) should be easy.  Some that might be
harder are: Fill Mode(Please, somebody figure this out!), rounded rectangles,
and any modes that are not directly supported by Tkinter.
Also, a save feature would be nice.
Question: Should an effort be made to change the drawing area to a actual
bitmap, rather than item-oriented like it is now?

                                        Jesse Weinstein
                                        jessw@loop.com
                                        Feb 19, 2001
"""
"""

Paint program by Dave Michell.

Subject: tkinter "paint" example
From: Dave Mitchell <davem@magnet.com>
To: python-list@cwi.nl
Date: Fri, 23 Jan 1998 12:18:05 -0500 (EST)

  Not too long ago (last week maybe?) someone posted a request
for an example of a paint program using Tkinter. Try as I might
I can't seem to find it in the archive, so i'll just post mine
here and hope that the person who requested it sees this!

  All this does is put up a canvas and draw a smooth black line
whenever you have the mouse button down, but hopefully it will
be enough to start with.. It would be easy enough to add some 
options like other shapes or colors...

						yours,
						dave mitchell
						davem@magnet.com
"""

from Tkinter import *
import os
"""paint.py: not exactly a paint program.. just a smooth line drawing demo."""
class Painter:
  def __init__(self):
    """Init variables and constants, then call make_interface.  This is the
    first part of the setup."""

    self.canvas_size=(500, 400)
    self.canvas_bg='white'
    self.modes=[PencilMode(), CircleMode(), RectangleMode()]
    self.colors=['red', 'blue', 'green', 'purple', 'orange', 'yellow',\
                 'darkgreen', 'darkblue']
    self.cnt_mode=self.modes[0]
    self.cnt_data={'color':self.colors[0], 'width':1}
    self.make_interface()

  def make_interface(self):
    """Create, or call for the creation of, all the parts of the interface.
    This is the second, and last, part of the setup."""
    self.root = Tk()
    self.root.title('Python Painter')

    #Make a menu.
    self.menu=Menu(self.root)
    self.menu.add_command(label='Save as', command=lambda x=self:SaveAsDlg(x))
    self.make_width_menu()
    self.root.config(menu=self.menu)

    #Create the widgets.
    self.drawing_area = Canvas(self.root, height=self.canvas_size[0],\
                               width=self.canvas_size[1], bg=self.canvas_bg)
    self.make_mode_area()
    self.make_color_area()

    #Grid them in.
    self.color_area.grid(row=1, column=1)
    self.mode_area.grid(sticky=N+S, row=0, column=0)
    self.drawing_area.grid(row=0, column=1)
    
    #Bind drawing bindings to general handlers.
    self.drawing_area.bind("<Motion>", self.motion)
    self.drawing_area.bind("<ButtonPress-1>", self.b1down)
    self.drawing_area.bind("<ButtonRelease-1>", self.b1up)
    
  def make_mode_area(self):
    """A Sub-function of make_interface.  Create a radiobutton for each mode in
    self.modes; use whatever arguments are listed in each mode's button_args
    attribute, then pack them all in a frame."""
    
    self.mode_var=IntVar(self.root)
    self.mode_area=Frame(self.root, relief=RIDGE, bd=2)
    for count in range(len(self.modes)):
      mode=self.modes[count]
      mode.button_args.update({'command':self.enter_mode, 'indicatoron':0, \
                               'variable':self.mode_var, 'value':count})
      mode.button=apply(Radiobutton, [self.mode_area], mode.button_args)
      mode.button.pack(fill=X)
      
  def make_color_area(self):
    """A Sub-function of make_interface. Create a radiobutton for each color
    in self.colors, then grid them in a frame.  The emptyImage is used to make
    each radiobutton take width and height args in pixels.  It's a really ugly
    hack, but it _does_ work."""
    self.color_var=IntVar(self.root)
    self.color_area=Frame(self.root)
    self.emptyImage=PhotoImage(master=self.color_area)
    for count in range(len(self.colors)):
      Radiobutton(self.color_area, indicatoron=0, command=self.change_color,\
                  bg=self.colors[count], selectcolor=self.colors[count],\
                  variable=self.color_var, value=count, image=self.emptyImage,\
                  width=30, height=20, bd=3).\
                  grid(row=0, column=count)
  def make_width_menu(self):
    """A Sub-function of make_interface.  Create a menu to allow the user to
    pick the line width.  Connect it to the main menu."""
    width_menu=Menu(self.menu)
    self.width_var=IntVar(self.menu)
    for count in range(1, 6):
      width_menu.add_radiobutton(variable=self.width_var, value=count, \
                                 label=`count`, command=self.set_width)
    self.menu.add_cascade(label='Line Width', menu=width_menu)
  def set_width(self):
    """Just apply the width_var to the correct place,"""
    self.cnt_data['width']=self.width_var.get()
  def change_color(self):
    """Apply the color identified by the color_var to the correct place."""
    self.cnt_data['color']=self.colors[self.color_var.get()]
  def enter_mode(self):
    """Call the enter_mode fucntion of the mode itentified by the mode_var, and
    assign the new mode to the correct place; but only if the mode has changed."""
    new_mode=self.modes[self.mode_var.get()]
    if new_mode != self.cnt_mode:
      new_mode.enter_mode()
      self.cnt_mode=new_mode
#General Handlers:  They simply call the appropriete mode handlers.
  def b1down(self, event):
    self.cnt_mode.b1down(event, self.cnt_data)
  def b1up(self, event):
    self.cnt_mode.b1up(event, self.cnt_data)
  def motion(self, event):
    self.cnt_mode.motion(event, self.cnt_data)
class SaveAsDlg:
  def __init__(self, painter_instence):
    self.up=painter_instence
    self.modes=[SaveAsTkinterProgram(self.up)]
    cnt_mode=0
    filename=raw_input('Name of file to save into: ')
    if os.access(filename, os.F_OK):
      if not raw_input('The file exists.  Overwrite? ')[0].lower()=='y':
        return
    f=open(filename, 'w')
    f.write(self.modes[cnt_mode].doIt())
    f.close()
class SaveAsTkinterProgram:
  colorARG={'line':'fill', 'oval':'outline', 'rectangle':'outline'}
  def __init__(self, painter_instence):
    self.menuitem_args={'label':'Python Tkinter Program'}
    self.up=painter_instence
  def doIt(self):
    cnvs=self.up.drawing_area
    ans=['"""This was created by paint.py\'s save function."""','',\
         'from Tkinter import *','','def ShowAPicture():','    root=Tk()',\
         'root.title("A Picture")']
    ans.append('    c=Canvas(root, width='+`self.up.canvas_size[0]`+\
               ', height='+`self.up.canvas_size[1]`+', bg='+`self.up.canvas_bg`+')')
    ans.extend(['    c.pack()', ''])
    for item in cnvs.find_all():
      str='    c.create_'+cnvs.type(item)+'('+`cnvs.coords(item)`[1:-1]+', '+\
           'width='+cnvs.itemcget(item, 'width')+', '+\
           self.colorARG[cnvs.type(item)]+'='+\
           `cnvs.itemcget(item, self.colorARG[cnvs.type(item)])`+')'
      ans.append(str)
    ans.extend(['if __name__ == "__main__":', '    ShowAPicture()', ''])
    return '\n'.join(ans)
class GeneralMode:
  """The base class of all modes.  This includes empty defs of all used
  functions, so derived classes can ingore some of them without harm."""
  def __init__(self):
    pass
  def b1down(self, event, data):
    pass
  def b1up(self, event, data):
    pass
  def motion(self, event, data):
    pass
  def enter_mode(self):
    pass
class RectangleMode(GeneralMode):
  """Rectangle drawing mode."""
  def __init__(self):
    self.button_args={'text':'Rectangle'}
    self.btn='up'
    self.pt0=(None, None)
    self.pt1=(None, None)
  def b1down(self, event, data):
    self.btn='down'
    self.pt0=(event.x, event.y)
    event.widget.create_rectangle(event.x, event.y, event.x, event.y,\
                             tag='tmp_rect', outline=data['color'],\
                             width=data['width'])
  def motion(self, event, data):
    if self.btn=='down':
      event.widget.coords('tmp_rect', self.pt0[0], self.pt0[1],\
                          event.x, event.y)
  def b1up(self, event, data):
    self.btn='up'
    event.widget.dtag('tmp_rect', 'tmp_rect')

class PencilMode(GeneralMode):
  """Free drawing mode.  The basic code for this mode came from the old paint
  program of dave mitchell's."""
  def __init__(self):
    self.button_args={'text':'Pencil'}
    self.b1 = "up"
    self.xold = None
    self.yold = None
  def b1down(self, event, data):
    self.b1 = "down"   # you only want to draw when the button is down
                       # because "Motion" events happen -all the time-
  def b1up(self, event, data):
    self.b1 = "up"
    self.xold = None # reset the line when you let go of the button
    self.yold = None
  def motion(self, event, data):
    if self.b1 == "down":
      if self.xold != None and self.yold != None:
        # here's where you draw it. smooth. neat.
        event.widget.create_line(self.xold,self.yold,event.x,event.y,\
                                 smooth=TRUE, fill=data['color'], \
                                 width=data['width'])
      self.xold = event.x
      self.yold = event.y

class CircleMode(GeneralMode):
  """The first mode I created.  It creates one oval, then changes its coords
  till you let the mouse btn up."""
  def __init__(self):
    self.button_args={'text':'Circle'}
    self.btn='up'
    self.pt0=(None, None)
    self.pt1=(None, None)
  def b1down(self, event, data):
    self.btn='down'
    self.pt0=(event.x, event.y)
    event.widget.create_oval(event.x, event.y, event.x, event.y,\
                             tag='tmp_circle', outline=data['color'],\
                             width=data['width'])
  def motion(self, event, data):
    if self.btn=='down':
      event.widget.coords('tmp_circle', self.pt0[0], self.pt0[1],\
                          event.x, event.y)
  def b1up(self, event, data):
    self.btn='up'
    event.widget.dtag('tmp_circle', 'tmp_circle')

class A_LaMode(GeneralMode):
  """For those who want to add ice-cream to their drawings. ;-)
  A test mode."""
  def __init__(self):
    self.button_args={'text':'A-La-Mode'}
  def enter_mode(self):
    print 'A La Mode!!!!'
    
if __name__ == "__main__":
  it=Painter()
#  it.root.mainloop()
