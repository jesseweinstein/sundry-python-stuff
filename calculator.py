"""A Simple calculator app with a GUI front end.

By Jesse Weinstein
    jessw@loop.com
    
Written for the "Python Challenge" at Useless Python.
Version 1.01

Changes from 1.00 or 'calculator.py':
   * Added a zero button.
   * Fixed integer problem by allowing a specific display_length.
   * A few small changes.
TODO:
    * Memory functions.
    * Other scientific calculator functions: e.g. powers, roots,
        factorials, etc.  I have a graphing tool that could be added, maybe.
    * Help?
    * Anything else you need.

First version written in __exactly__ 1 hour.
Version 1.00 released on June 24, 2001, at 2:42 PM, Pacific Standard Time
Version 1.01 released on June 25, 2001, at 11:33 PM, Pacific Standard Time
"""

from Tkinter import *

class Calculator:
    def __init__(self, font_size=12, display_length=9):
        if display_length < 7 or display_length > 9:
            raise Exception, 'display_length is out of bounds!'
        self.display_length=display_length
        self.font_size=font_size
        self.make_window()
    def make_window(self):
        self.root=Tk()
        self.root.title('Calculator')
        self.opersF=Frame(self.root)
        self.opers=[]
        for item in ['+', '-', '*', '/']:
            self.opers.append(Button(self.opersF, text=item[0],\
                                     height=1, width=2, fg='blue',\
                                     font=('', `self.font_size`, '')))
            self.opers[-1].bind('<1>', self.buttonCB)
            self.opers[-1].pack(side=TOP)

        self.display=Entry(self.root, font=('', `self.font_size`, ''),\
                           state=DISABLED, width=self.display_length)
        
        self.numsF=Frame(self.root)
        self.nums=[]
        for item in range(1, 10):
            self.nums.append(Button(self.numsF, text=`item`,\
                                    height=1, width=2, \
                                    font=('', `self.font_size`, '')))
            self.nums[-1].bind('<1>', self.buttonCB)
            self.nums[-1].grid(column=(item-1) % 3, row=(item-1)/3)
        self.othersF=Frame(self.root)
        self.others=[]
        for item in [('=', self.equals, 'white'),\
                     ('C', self.clear, 'red'),\
                     ('d', self.backspace, 'red')]:
            self.others.append(Button(self.othersF, text=item[0], fg=item[2],\
                                      command=item[1], height=1, width=2,
                                      font=('', `self.font_size`, '')))
        self.others.insert(1, Button(self.othersF, text='0', height=1, width=2,\
                                  font=('', `self.font_size`, '')))
        self.others[1].bind('<1>', self.buttonCB)
        for item in self.others:
            item.pack(side=RIGHT)
        
        self.display.grid(column=0, row=0, columnspan=3, sticky=SE)
        self.opersF.grid(column=3, row=0, rowspan=2, sticky=W)
        self.numsF.grid(column=0, row=1, columnspan=3, sticky=SE)
        self.othersF.grid(column=0, row=2, columnspan=4, sticky=NE)
    def buttonCB(self, event):
        if len(self.display.get())==self.display_length:
            return
        val=event.widget.cget('text')
        self.display.config(state=NORMAL)
        self.display.insert(END, val)
        self.display.config(state=DISABLED)
    def equals(self):
        if self.display.get():
            ans=`eval(self.display.get())`
            self.display.config(state=NORMAL)
            self.display.delete(0, END)
            if len(ans) > self.display_length:
                self.display.insert('Too Big')
            else:
                self.display.insert(0, ans)
            self.display.config(state=DISABLED)
    def clear(self):
        self.display.config(state=NORMAL)
        self.display.delete(0, END)
        self.display.config(state=DISABLED)
    def backspace(self):
        self.display.config(state=NORMAL)
        self.display.delete(len(self.display.get())-1)
        self.display.config(state=DISABLED)

def IDLEtest():
    """Returns 1 when IDLE is running, 0 else.
    Please let me know (through the IDLE-devl list) if you find a situation
    where this gives the wrong answer."""
    import sys
    try:
        if sys.stdin.__module__=='PyShell':
            return 1
        else:
            return 0
    except AttributeError:
        return 0
        
if __name__=='__main__':
    it=Calculator()
    if not IDLEtest():
        it.root.mainloop()
