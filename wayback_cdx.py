"""
    Wayback_CDX -- a wrapper around the Wayback Machine's CDX index interface.
    Copyright (C) 2016 Jesse Weinstein <jesse@wefu.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import absolute_import
import datetime

import simplejson
import requests

WAYBACK_URL = u'https://web.archive.org/web/{0[timestamp]}{1}/{0[original]}'
BASE_URL = u'http://web.archive.org/cdx/search/cdx'
TIMESTAMP_FORMAT = '%Y%m%d%H%M%S'


def get(url, **kwargs):
    kwargs.setdefault('from', kwargs.get('from_'))

    def convert_dates(key):
        now = datetime.datetime.now()
        val = kwargs.get(key)
        if isinstance(val, datetime.time):
            dt = datetime.datetime.combine(now, val)
        # elif isinstance(val, datetime.date):
        #     dt = datetime.datetime.combine(val, now)
        elif isinstance(val, (datetime.datetime, datetime.date)):
            dt = val
        else:
            return
        kwargs[key] = dt.strftime(TIMESTAMP_FORMAT)

    def is_true(key):
        return str(kwargs.get(key)).lower() == 'true'
    convert_dates('to')
    convert_dates('from')
    fl = kwargs.get('fl')
    if isinstance(fl, (list, tuple, set, frozenset)) and isinstance(iter(fl).next(), str):
        kwargs['fl'] = ','.join(fl)

    kwargs.update({'url': url, 'output': 'json'})
    response = requests.get(BASE_URL, kwargs)
    if is_true('returnBareResponse'):
        return response

    if is_true('showNumPages'):
        return [{'numPages': int(response.text)}]
    try:
        rtn = response.json()
    except simplejson.JSONDecodeError:
        print 'Unable to decode as JSON'
        return response
    if is_true('showResumeKey'):
        extra = [{'resumeKey': rtn[-1][0]}]
        rtn = rtn[:-2]
    else:
        extra = []
    rtn_as_dicts = [dict(zip(rtn[0], x)) for x in rtn[1:]]
    if rtn_as_dicts and 'timestamp' in rtn_as_dicts[0]:
        for d in rtn_as_dicts:
            d['datetime'] = datetime.datetime.strptime(
                d['timestamp'], TIMESTAMP_FORMAT)
            if d.get('original'):
                d['waybackurl'] = WAYBACK_URL.format(d, "")
                d['waybackurl_id'] = WAYBACK_URL.format(d, "id_")

    return rtn_as_dicts + extra


def get_urls(base_url, **kwargs):
    kwargs.setdefault('matchType', 'prefix')
    kwargs.setdefault('collapse', 'urlkey')
    kwargs.setdefault('fl', 'original')
    return [x.get('original') for x in get(base_url, **kwargs)]
