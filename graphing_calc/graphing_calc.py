"""A module implementing a mathamatical graph generator.

Usage:
    * Import the module; a Tk window will appear. (If you accidently destroy the
        window, you can restore it with graphing_calc.make_interface())
    * If you want to change any parameters, use graphing_calc.change().
    * To graph an equation, use graphing_calc.a_graph().
For specific information on each function, see appropriete doc string.

                                            Created by Jesse Weinstein
                                            jessw@netwood.net
                                            on Feb 20, 2001
"""
"""
----Interesting graphs----

graphing_calc.a_graph('qq(x, 4,0,0)', {'qq':lambda x,h,v,r:[q(x, r, h)+v, -q(x, r, h)+v], 'q':lambda x, r, h:math.sqrt((r-((x-h)**2)))}, 'red')
#Non-functional circle graph.  To be worked on.-3/30/03 12:41 am
graphing_calc.a_graph('+'.join(\
    ['(math.sin(x*'+`n`+')/'+`n`+')' for n in range(1, 60, 2)]))
#This creates a square wave by adding 30 sin funcitons together.

f=0.5;m=10;graphing_calc.a_graph('a(x)*math.sin(x*f)', {'f':f, 'a':graph.Sometimes(\
    lambda :graphing_calc.random.random()*m+1, graphing_calc.math.pi/f, graphing_calc.get('stp')).it})
#This creates a sin wave with amplitudes that change with every pass through
#zero.  New values are a random integer between 1 and m+1.

graphing_calc.a_graph("x%(math.pi/f)<get('stp') and 9 or -9", {'f':f}, 'yellow')
#This creates vertical yellow lines at each point where the amplitude changes
#in the above equation.
"""
from Tkinter import *
import sys, types

import math, cmath, random
#So equations can use math functions without needing to call the module in
#their own code.

#TODO: Add error handler for accidently deleting the graphing_calc window.  Right now
#it will just cough and die.-April 29, 2002
#DONE: If you enter one value when a option expects 2 or more, it will put the same
#into all of them.
class Grapher:
    def __init__(self):
        #TODO:Fix descriptions
        #TODO:Add check for correct number of items in apply_changes
        self._option_dict={'scale':('X and Y scale, in pixels per unit', 2),
                           'rng':('Range of X values to input into equations.'
                                  ' (Set to visble area by default)', 2),
                           'stp':('Distence between X values input into equations.', None),
                           'mark_stp':('Distence between marks on X and Y axis,'
                                       ' in units.', 2),
                           'ctr':('Position of the center of the graph, in'
                                  ' fractions of size', 2),
                           'size':('The size of the graphing window, in pixels', 2),
                           'range_color':('Color of range markers.', None),
                           'mark_color':('Color of marks.', None),
                           'axis_color':('Color of axies.', None),
                           'pt_radius':('Radius of points(drawn with the draw_lines'
                                        ' arg off', None),
                           'quiet':('Hide non-fatal error messages.', None),
                           'draw_lines':('Connect generated points with lines.', None)}
        
        self._cnt_equations={} #A dict, keyed by color, with tuples of
        #equations and dicts of user's paramaters as values.
        self.scale=[0,0]
        self.rng=[0,0]
        self.size=[0,0]
        self.ctr=[0,0]
        self.stp=0
        self.mark_stp=[0, 0]
        self.range_color=''
        self.mark_color=''
        self.axis_color=''
        self.pt_radius=2
        self.quiet=0
        self.draw_lines=1
        self.reset()
    def reset(self):
        self.make_interface()
        self.apply_change(scale=[50,50], rng=[0, 9.0], stp=0.1,\
                          size=[500, 500], ctr=[.01, .99], mark_stp=[0.1, 0.1],\
                          range_color='red', mark_color='red', axis_color='black')
    def make_interface(self):
        """Create the widgets."""
        self._root=Tk()
        self._root.title('graphing_calc')
        self._c=Canvas(self._root, width=200, height=200)
        self._c.pack()
    def calc_range(self):
        neg=(self.ctr[0]*self.size[0])/self.scale[0]
        pos=(self.size[0]-neg)/self.scale[0]

        return [-neg, pos]
    def apply_change(self, *args, **keyargs):
        """See the doc string for the change function."""

        #--First, parse and arrange the args--
        #****************************************
        
        return_text=[]
        if args==('options',) : #handle the option list display
            return_text.append('---List of options---')
            for key in self._option_dict.keys():
                keyargs[key]=None

        for key in keyargs.keys(): #handle the keyargs
            if self._option_dict.has_key(key):

                tmp=getattr(self, key)

                if keyargs[key]==None: #handle the "None" shortcut
                    return_text.append(key+':'+`tmp`+': '+self._option_dict[key][0])
                    continue

                val=keyargs[key]

                #handle incorrect #'s of items given for an option
                if isinstance(val, (types.ListType, types.TupleType)):
                    lenval=len(val)
                else:
                    lenval=None
                lendes=self._option_dict[key][1]

                print `key, lendes, val, lenval`
                
                if lendes == None and lenval != None:
                    val=val[0]
                    print 'Warning: '+key+' expects a single value;'+\
                          ' you provided a sequence.  The first item'+\
                          ' was used.'
                elif lendes != None and lenval == None:
                    val=(val,)*lendes 
                    print 'Warning: '+key+' expects a tuple of '+`lendes`+\
                          ' items; you provided a single value.  The value'+\
                          ' was used for all the expected items.'
                elif lendes != lenval:
                    val=val[:lendes]
                    print 'Warning: '+key+' expects a tuple of '+`lendes`+\
                          ' items; you provided a sequence of '+`lenval`+\
                          ' items.  The first '+`lendes`+' items were used.'
                    
                setattr(self, key, val)
                if key=='size' or key=='ctr' or key=='scale' and 'rng' not in keyargs.keys():
                    self.rng=self.calc_range()
            else:
               return_text.append("Error: "+key+" is not a known option."
                                  " (Call graphing_calc.change('options') for a list)")
        if return_text:
            return '\n'.join(return_text)

        #--Now, do the drawing that's necessary...--
        #**************************************
        
        #First, (re)set the window size:
        self._c.config(width=self.size[0], height=self.size[1])
        
        #Now, (re)draw all the guide marks.
        self._c.delete(ALL)

        #Step marks
        for count in range(2): #For X and Y axes.
            pos=(self.ctr[count]*self.size[count]) % (self.scale[count]*self.mark_stp[count])
            
            while pos <= self.size[count]:
                if count == 0:
                    self._c.create_line(pos, (self.ctr[1]*self.size[1])-5, pos, (self.ctr[1]*self.size[1])+5,\
                                       fill=self.mark_color, tag='marks')
                else:
                    self._c.create_line((self.ctr[0]*self.size[0])-5, pos, (self.ctr[0]*self.size[0])+5, pos,\
                                       fill=self.mark_color, tag='marks')
                pos += self.mark_stp[count]*self.scale[count]

        #X axis and Y axis lines
        self._c.create_line(0, (self.ctr[1]*self.size[1]), self.size[0],\
                            (self.ctr[1]*self.size[1]), fill=self.axis_color,\
                            tag='marks')
        self._c.create_line((self.ctr[0]*self.size[0]), self.size[1],\
                            (self.ctr[0]*self.size[0]), 0, fill=self.axis_color,\
                            tag='marks')
        
        #Range marks(in red)
        for count in range(2):
            self._c.create_line(self.rng[count]*self.scale[0]+\
                                (self.ctr[0]*self.size[0]),\
                               (self.ctr[1]*self.size[1])-5,\
                               self.rng[count]*self.scale[0]+\
                                (self.ctr[0]*self.size[0]),\
                               (self.ctr[1]*self.size[1])+5, fill=self.range_color,\
                                tag='marks')

        #Pre-calculated values
        self.offset=[self.ctr[n]*self.size[n] for n in [0,1] ]
        
        if self._cnt_equations:#redraw the cnt_equations, if there are any.
            for (color, (equ, params)) in self._cnt_equations.items():
                self.graph(equ, params, color) 

    def graph(self, equ='Same as before', params='No user params', color='black'):
        """See the doc string for the a_graph function."""

        #Handle arguments
        if params=='No user params':
            params={}
        if equ=='Same as before':
            if color in self._cnt_equations.keys():
                equ=self._cnt_equations[color]
            else:
                raise Exception, 'No equation given, and no equation with that'\
                      ' color in memory!'
        self._cnt_equations.update({color:(equ, params)})

        for ref in params.values():
            if type(ref) is ListType:
                key=params.keys()[params.values().index(ref)]
                for item in ref:
                    params[key]=item
                    self.graph(equ, params, color)
                    if raw_input('Show next value?') == 'n':
                        return
                return
        #Now, generate the points...
        ans=[]
        x=float(self.rng[0])
        lo=locals(); lo.update(params) #add your params.
#        print 'point gen'
        while x <= self.rng[1]:
            try:
                lo.update({'x':x})
                y=eval(equ, globals(), lo)
#                print y
            except NameError:
#                print sys.exc_info()
                sys.stderr.write('ERROR: Non-defined parameter used in given '\
                      'equation; please re-graph with graphing_calc.a_graph and '\
                      'include the parameter.')
                break
            except ZeroDivisionError:
                if not self.quiet: print 'Division by zero at X:', x, 'Ignoring.'
            except ValueError:
                if sys.exc_info()[1].args[0]=='math domain error':
                        if not self.quiet: print 'Math Domain Error at X:',x,'Ignoring.'
                else:
                    sys.excepthook(*sys.exc_info())
            except StopIteration:
                print 'No more values avaliable at X:', x, 'Stopping point generation.'
                break

            else:
                if y==None:
                    if not self.quiet: print 'No Value at X:', x, 'Ignoring.'
                else:
                    the_real_x=x*self.scale[0]+self.offset[0]
                    if type(y)==types.ListType or type(y)==types.TupleType:
                        for ay in y:
                            ans.append((the_real_x,\
                                        -ay*self.scale[1]+self.offset[1]))
                    else:
                        ans.append((the_real_x,\
                                    -y*self.scale[1]+self.offset[1]))
            x += self.stp
#        print 'drawing', ans
        #And draw them.
        self._c.delete('graph:'+color)
        if self.draw_lines:
            for count in range(1, len(ans)):
                self._c.create_line(ans[count][0],\
                                   ans[count][1],\
                                   ans[count-1][0],\
                                   ans[count-1][1],\
                                   fill=color, tag='graph:'+color)
        else:
            for pt in ans:
                self._c.create_oval(pt[0]-self.pt_radius,\
                                    pt[1]-self.pt_radius,\
                                    pt[0]+self.pt_radius,\
                                    pt[1]+self.pt_radius,\
                                    fill=color, tag='graph:'+color)
                
    def clear(self, color):
        self._c.delete('graph:'+color)
        if color in self._cnt_equations.keys():
            del self._cnt_equations[color]
    def getdoc(self, option):
        return self._option_dict.get(option, 'error')
    def get(self, option):
        if not self._option_dict.has_key(option):
            return 'error'
        return getattr(self, option, 'error')
HELP_TEXT="""
This is a program to draw graphs.  Here is a list of
the functions available, and what they do. Each can be used by
typing graphing_calc.<name of function>(<arguments, if any>)  Documentation
on each can be accessed by calling them with no arguments.

    help -- You're looking at it.
    a_graph -- The most important function.  Makes graphs.
    change -- The second most important function.  Allows you to change
        the options for the graphing window (Get a list of options by
        calling it with no args) (Get docs on one option by setting it to None).
    make_interface -- This recreates the graphing_calc window.
"""
def help():
    print HELP_TEXT
def clear(color):
    _private_graph_instance.clear(color)
def reset():
    """This recreates the graphing_calc window if you have accidently deleted it."""
    _private_graph_instance.reset()
def get(option):
    tmp=_private_graph_instance.get(option)
    if tmp=='error':
        raise AttributeError, 'This is not a known option! (Call graphing_calc.change() for help)'
    return tmp
def a_graph(equation='Same as before', your_params='No user params', color='black'):
    """This makes graphs.

        The equation should be a valid Python expression as a string
    (see Python docs for more info).  The most common trap is that
    exponentiation is spelled '**', so x squared would be spelled 'x**2'.
        The variable 'x' will be defined, but any other variables you use
    should be listed in your_params as dictionary entries(like the dicts
    returned by globals() and locals()).
        The color arg can be given to specify the color of the graph to draw.
    Only one graph of each color will be shown at once."""
    if equation=='Same as before' and your_params=='No user params' and color=='black':
        print '---Documentation for graphing_calc.a_graph---'
        print a_graph.__doc__
        return
    return _private_graph_instance.graph(equation, your_params, color)
def change(*args, **keyargs):
    """This changes the graphing window's options.
    
        To get a list of options, call graphing_calc.change('options').
    To set an option, enter its name, an equals sign, then
    the value you want to assign to it.  To change more than
    one option, repeat the above, seperating each item by a comma.
    To access help on an option, set it to the word "None"(no quotes)"""
    if args==() and keyargs=={}:
        print '---Documentation for graphing_calc.change---'
        print change.__doc__
    tmp=_private_graph_instance.apply_change(*args, **keyargs)
    if type(tmp)==type(''):
        print tmp
    else:
        return tmp

#--Other Interesting Things---
class Sometimes:
    def __init__(self, func, every, closeEnough):
        """Use "func()" to update the return value of "Sometimes.it(n)"
        every time "n" is "closeEnough" to a multiple of "every". """
        self.func=func
        self.every=every
        self.closeEnough=closeEnough
        self.it(self.every)
    def it(self, n):
        if n % self.every < self.closeEnough:
            self.val=self.func()
        return self.val
def main():
    global _private_graph_instance
    _private_graph_instance=Grapher()
    print 'Call graphing_calc.help() for info on how to use graphing_calc'

if __name__=='__main__':
    print 'This module should be imported, not run.  Please open up a Python shell amd type \'import graph\' to use this program.'
    raw_input('Press any key when done reading...')
else:
    main()
"""Changelog:
*Most of the program was written before I started making numbered versions.
1.0 First numbered version.  Only available without any identification.
1.1 Last released version.
    Added raw_input to stop the don't import message from vanishing before it
    could be read.
1.2 The current version... (Including any differences between graphing_calc.py and
graph11.py
"""
class PopFormula:
	def __init__(self, start, r):
		self.p=start
		self.r=r
	def next(self):
		self.p=self.p*self.r*(1-self.p)
		return self.p
def foo(r, dir=1):
	graphing_calc.a_graph('q.next()', {'q':PopFormula(0.2, r)})
	if r>=4:
		dir=-1
	if r<=3:
		return
	graphing_calc._private_graph_instance._root.after(10, lambda :foo(r+(dir*0.01), dir))

class StarsDataHandler:
    def __init__(self, filename):
        txt=open(filename, 'r').read()
        txt=txt[txt.find('DATA BEGINS')+len('DATA BEGINS')+1:]
        print txt
        b=[[float(y) for y in x.replace(',' ,'').split(' ') if y] for x in txt.splitlines()]
        c=[[x[3]]+x[:3] for x in b]
        maxmins=[[None, 0.12, -200, 0], [None, 8.0, 200, 100]]
        nc=[ normalize([x[n] for x in c+maxmins]) for n in range(1, 4)]
        rnc=[[c[n][0], nc[0][n], nc[1][n], nc[2][n]] for n in range(len(c))]
        self.d=[]
        for n in range(1, 4):
            self.d.append({})
            for x in rnc:
		unused=self.d[n-1].setdefault(x[0], [])
		self.d[n-1][x[0]].append(x[n])
		self.d[n-1][x[0]].sort()
    def draw(self):
        change(ctr=[0.3, 0.99], stp=1, mark_stp=[10, 0.1],\
                     scale=[10,475], size=[1000, 500], pt_radius=0.5, draw_lines=0)
        a_graph('d.get(x, -5)', {'d':self.d[2]}, 'darkgreen')
        a_graph('d.get(x, -5)', {'d':self.d[1]}, 'red')
        a_graph('d.get(x, -5)', {'d':self.d[0]}, 'blue')
    def draw_edges(self):
        """Draw lines where at max and min of humanoid tolerances."""
        
        human=[[None, 0.012690355329949238, 0.15999999999999999, 0.14999999999999999],\
               [None, 0.54314720812182737, 0.85999999999999998, 0.84999999999999998]]
        #the second and the third are actually identical, but I modified them for reasons of
        #clarity.
        a_graph('h', {'h':human[0][1]}, 'Blue')
        a_graph('h', {'h':human[0][2]}, 'Red')
        a_graph('h', {'h':human[0][3]}, 'Darkgreen')
        a_graph('h', {'h':human[1][1]}, 'BluE')
        a_graph('h', {'h':human[1][2]}, 'ReD')
        a_graph('h', {'h':human[1][3]}, 'DarkgreeN')
    def clear_edges(self):
        clear('Blue')
        clear('Red')
        clear('Darkgreen')
        clear('BluE')
        clear('ReD')
        clear('DarkgreeN')
    def it(self, x):
        return int(x) in self.keys and self.data[int(x)] or None
class StarsDataHandler2:
    def __init__(self, txt):
        axis2=3
        semi_data=[(float(x[0:4]), int(x[6:10]), int(x[12:14]), int(x[18:21]))\
                   for x in txt.splitlines() ]
        for n in range(3):
            semi_data=self.normalize(semi_data, n)
#        print semi_data
        self.data={}
        for line in semi_data:
            self.data.setdefault(line[axis2], [])
            self.data[line[axis2]].append(line[0]+line[1])
        self.keys=self.data.keys()
    def normalize(self, d, axis):
        d2=[ [ d[n][c] for n in range(len(d)) ] for c in range(len(d[0])) ]
        d2[axis]=normalize(d2[axis])
        return [ [ d2[n][c] for n in range(len(d2)) ] for c in range(len(d2[0])) ]
    def it(self, x):
        return int(x) in self.keys and self.data[int(x)] or None
class Once:
	def __init__(self, init_func, func=lambda a,b,t:t, inital=None):
		self.func=func
		self.init_func=init_func
		self.thing=inital
	def __call__(self, a,b):
		if a==b:
			self.thing=self.init_func(a,b)
		return self.func(a,b, self.thing)
def normalize(data):
    mi=min(data); mx=max(data); q=1.0/(mx-mi)
    return [(x-mi)*q for x in data]    
