"""A module implementing a mathamatical graph generator.

Usage:
    * Import the module; a Tk window will appear. (If you accidently destroy the
        window, you can restore it with graphing_calc.make_interface())
    * If you want to change any parameters, use graphing_calc.change().
    * To graph an equation, use graphing_calc.a_graph().
For specific information on each function, see appropriete doc string.
"""
import sys

classifiers = """\
Development Status :: 4 - Beta
Intended Audience :: Developers
Intended Audience :: Education
Intended Audience :: End Users/Desktop
Intended Audience :: Science/Research
License :: OSI Approved :: MIT License
Natural Language :: English
Operating System :: OS Independent
Programming Language :: Python
Topic :: Education
Topic :: Scientific/Engineering
Topic :: Scientific/Engineering :: Mathematics
Topic :: Scientific/Engineering :: Visualization
"""
name='graphing_calc';version='1.1'
doc=__doc__.split("\n")

# patch distutils if it can't cope with the "classifiers" or
# "download_url" keywords
if sys.version < '2.2.3':
    from distutils.dist import DistributionMetadata
    DistributionMetadata.classifiers = None
    DistributionMetadata.download_url = None

from distutils.core import setup
setup(\
    name=name,
    version=version,
    author="Jesse Weinstein",
    author_email="http://purl.org/NET/JesseW/email@purl.org",
    maintainer="Jesse Weinstein",
    maintainer_email="jessw@netwood.net",
    url="http://purl.oclc.org/NET/JesseW/Python",
    download_url="http://purl.oclc.org/NET/JesseW/SundryStuff/"\
    +name+"-"+version+".zip",
    license="http://www.opensource.org/licenses/mit-license.html",
    description=doc[0],
    long_description="\n".join(doc[2:]),
    classifiers = filter(None, classifiers.split("\n")),
    py_modules=[name]
    )
