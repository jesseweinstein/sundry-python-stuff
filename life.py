"""This is a Conway's Life generator.

I wanted to generate life patterns in a variaety of ways, so I needed
a scripting ability.  So I wrote this.  Comments, questions and patches can be sent to jessw@netwood.net"""
from Tkinter import *
import types, time
ON_CHAR='x' #this is the character recognized by the string2tuple method as lit.
class LifePattern:
    def __init__(self, pattern):
        """This is a class for generating Conway's Life patterns.
            The class includes a method to generate a given number of
            generations, and a display method with text based and tinker based
            displays."""
        if type(pattern) == types.StringType:
            pattern=self.string2tuple(pattern)
        self.start_pattern=pattern
        self.pattern=pattern
        self.first=1

        ptn=self.pattern
        for item in ptn:
            if ptn.count(item) > 1:
                ptn.remove(item)
        
    def __add__(self, object):
        if type(object)==types.StringType: #string format
            return self.__class__(self.pattern+self.string2tuple(object))
        elif `self`.split()[0] == `object`.split()[0]: #object format
            return self.__class__(self.pattern+object.pattern)
        elif len(object) and len(object[0]) == 2: #tuple format
            return self.__class__(self.pattern+object)
        else:
            raise TypeError, 'cannot add type "'+type(object).__name__+'" to LifePattern'
    def shifted(self, delta_x=0, delta_y=0):
        #TODO: check speed using map, maybe with operator.add?
        answer=[]
        for item in self.pattern:
            answer.append((item[0]+delta_x, item[1]+delta_y))
        return self.__class__(answer)
    def generate(self, numOfGenerations=1, debug=0):
        """This method is the generator. (Obviously ;-))  If the debug
        option is turned on, then it will print various bits of information
        about each point as it computes it. """
        for count in range(numOfGenerations):
            pattern=self.pattern[:]
            for point in pattern:
                neighbors=self.num_neighbors(point, pattern)
                if debug:
                    print point, neighbors, "------------"
                for x in (-1, 0, 1):
                    for y in (-1, 0, 1):
                        if not (x==0 and y==0) and self.pattern.count((point[0]+x, point[1]+y))==0:
                            temp=self.num_neighbors((point[0]+x, point[1]+y), pattern)
                            if debug:
                                print (point[0]+x, point[1]+y), temp
                            if temp == 3:
                                self.pattern.append((point[0]+x, point[1]+y))
                if not (neighbors==2 or neighbors==3):
                    self.pattern.remove(point)
    def go(self, numGenerations=1, delay_time=2, type='tk', squaresize=30, grid=0, debug=0):
        for x in range(numGenerations):
            self.generate(1, debug)
            self.display(type, squaresize, grid)
            if not self.pattern or self.first: #if first is set, it means that the dialog was closed.
                return
            start_t=time.time()
            while time.time()-start_t < delay_time:
                pass
    def __close(self, event=None):
        self.first=1
        self.root.destroy()
    def display(self, type='text', squaresize=30, grid=0):
        """This method can display the pattern in two ways.  If the type is set
        to 'text'(the default), then it will print the pattern, includeing metrics
        on the sides.  If the type is 'tk', then it will display the pattern in a
        window.  The size of the the squares in the tk view can be set with the
        squaresize option."""
        if type=='tk':
            if self.first:
                self.root=Tk()
                self.root.protocol("WM_DELETE_WINDOW", self.__close)
                self.first=0
                if not self.pattern:
                    self.c=Canvas(self.root, bg='white', width=150, height=50)
                else:
                    self.c=Canvas(self.root, bg='white', width=(max(map(lambda x:x[0], self.pattern))+1)*squaresize,\
                             height=(max(map(lambda x:x[1], self.pattern))+1)*squaresize)
                self.c.pack()

            else:
                if self.pattern:
                    self.c.config(width=(max(map(lambda x:x[0], self.pattern))+1)*squaresize,\
                                  height=(max(map(lambda x:x[1], self.pattern))+1)*squaresize)
                self.c.delete(ALL)
            if not self.pattern:
                self.c.create_text(20, 10, font=('', '14', ''),\
                                   anchor=NW,text='No Pattern!')
                return
            for point in self.pattern:
                self.c.create_rectangle(point[0]*squaresize, point[1]*squaresize,\
                                        (point[0]+1)*squaresize, (point[1]+1)*squaresize,\
                                        fill='blue')
            self.c.update()            
        elif type=='text':
            if self.pattern:                 
                text='   '
                for x in range(min(map(lambda x:x[0], self.pattern))-1, max(map(lambda x:x[0], self.pattern))+2):
                    text=text+'%2i' % x
                text=text+'\n'     
                for y in range(min(map(lambda x:x[1], self.pattern))-1, max(map(lambda x:x[1], self.pattern))+2):
                    text=text+'%2i' % y+'|'
                    for x in range(min(map(lambda x:x[0], self.pattern))-1, max(map(lambda x:x[0], self.pattern))+2):
                        #print x, y
                        if self.pattern.count((x, y)) == 1:
                            text=text+' X'
                        else:
                            text=text+' 0'
                    text=text+'\n'
            else:
                text='No Pattern!'
	    print text

    def string2tuple(self, pattern):
        """This method returns a list of tuples describing the pattern given as a string,

        The string format begins with a optional x,y offset, terminated by a :
        then the pattern is expressed with 'x'(default, can be changed)
        representing a lit square, and anything else representing a unlit
        square.  Newline's represent a new line in the pattern."""
        parts=pattern.split(':')
        if len(parts) == 2:
            offset=map(int, pattern[:pattern.index(':')].split(','))
        elif len(parts) == 1:
            offset=[0, 0]
            parts.insert(0, [])
        else:
            raise StandardError, 'The String Pattern given has too many ":"!'
        answer=[]
        x, y=offset
        for line in parts[1].split('\n'):
            for char in line:
                    if char == ON_CHAR:
                            answer.append((x, y))
                    x=x+1
            x=offset[0]
            y=y+1
        return answer
    def num_neighbors(self, point, pattern):
        """This is a helper method, used by the generate method"""
        neighbors=0
        for x in (-1, 0, 1):
                for y in (-1, 0, 1):
                    if not (x==0 and y==0):
                        #print "->", point[0]+x, point[1]+y
                        if pattern.count((point[0]+x, point[1]+y)) == 1:
                            neighbors=neighbors+1
        return neighbors
lg=LifePattern
