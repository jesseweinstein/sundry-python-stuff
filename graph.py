"""A module implementing a mathamatical graph generator.

Usage:
    * Import the module; a Tk window will appear. (If you accidently destroy the
        window, you can restore it with graph.make_interface())
    * If you want to change any parameters, use graph.change().
    * To graph an equation, use graph.a_graph().
For specific information on each function, see appropriete doc string.

                                            Created by Jesse Weinstein
                                            jessw@loop.com
                                            on Feb 20, 2001
"""
"""
----Interesting graphs----
graph.a_graph('+'.join(\
    ['(math.sin(x*'+`n`+')/'+`n`+')' for n in range(1, 60, 2)]))
#This creates a square wave by adding 30 sin funcitons together.

f=0.5;m=10;graph.a_graph('a(x)*math.sin(x*f)', {'f':f, 'a':graph.Sometimes(\
    lambda :graph.random.random()*m+1, graph.math.pi/f, graph.get('stp')).it})
#This creates a sin wave with amplitudes that change with every pass through
#zero.  New values are a random integer between 1 and m+1.

graph.a_graph("x%(math.pi/f)<get('stp') and 9 or -9", {'f':f}, 'yellow')
#This creates vertical yellow lines at each point where the amplitude changes
#in the above equation.
"""
from Tkinter import *
import sys, types

import math, cmath, random
#So equations can use math functions without needing to call the module in
#their own code.

#TODO: Add error handler for accidently deleting the PyGraph window.  Right now
#it will just cough and die.-April 29, 2002
class Grapher:
    def __init__(self):
        #TODO:Fix descriptions
        #TODO:Add check for correct number of items in apply_changes
        self._option_dict={'scale':'X and Y scale, in pixels per unit', \
                           'rng':'Range of X values to input into equations.'\
                           ' (Set to visble area by default)',\
                           'stp':'Distence between X values input into equations.',\
                           'mark_stp':'Distence between marks, in units.',\
                           'ctr':'Position of the center of the graph, in'\
                           ' fractions of size',\
                           'size':'The size of the graphing window, in pixels',\
                           'range_color':'Color of range markers.',\
                           'mark_color':'Color of marks.',\
                           'axis_color':'Color of axies.'}
        self._cnt_equations={} #A dict, keyed by color, with tuples of
        #equations and dicts of user's paramaters as values.
        self.scale=[0,0]
        self.rng=[0,0]
        self.size=[0,0]
        self.ctr=[0,0]
        self.stp=0
        self.mark_stp=0
        self.range_color=''
        self.mark_color=''
        self.axis_color=''
        self.reset()
    def reset(self):
        self.make_interface()
        self.apply_change(scale=[50,50], rng=[0, 9.0], stp=0.1,\
                          size=[500, 500], ctr=[.01, .99], mark_stp=0.1,\
                          range_color='red', mark_color='white', axis_color='black')
    def make_interface(self):
        """Create the widgets."""
        self._root=Tk()
        self._root.title('PyGraph')
        self._c=Canvas(self._root, width=200, height=200)
        self._c.pack()
    def calc_range(self):
        neg=(self.ctr[0]*self.size[0])/self.scale[0]
        pos=(self.size[0]-neg)/self.scale[0]

        return [-neg, pos]
    def apply_change(self, *args, **keyargs):
        """See the doc string for the change function."""
        ans=[]
        if args==('options',) : #handle the option list display
            ans.append('---List of options---')
            for key in self._option_dict.keys():
                keyargs[key]=None

        for key in keyargs.keys(): #handle all other options
            if self._option_dict.has_key(key):
                tmp=getattr(self, key)
                if keyargs[key]==None:
                    ans.append(key+':'+`tmp`+': '+self._option_dict[key])
                    continue
##                if getattr(tmp, '__len__', lambda :'no len')() != getattr(keyargs[key], '__len__', lambda :'no len')():
##                    print getattr(tmp, '__len__', lambda :'no len')(), getattr(keyargs[key], '__len__', lambda :'no len')()
##                    ans.append('Error: New value for '+key+' has wrong length!  Current value:'+`tmp`)
##                    continue
                setattr(self, key, keyargs[key])
                if key=='size' or key=='ctr' or key=='scale':
                    self.rng=self.calc_range()
            else:
               ans.append("Error: "+key+" is not a known option. (Call graph.change('options') for a list)")
        if ans:
            return '\n'.join(ans)
        #First, (re)set the window size:
        self._c.config(width=self.size[0], height=self.size[1])
        
        #Now, (re)draw all the guide marks.
        self._c.delete(ALL)

        #Step marks
        for count in range(2): #For X and Y axes.
            pos=(self.ctr[count]*self.size[count]) % (self.scale[count]*self.mark_stp)
            
            while pos <= self.size[count]:
                if count == 0:
                    self._c.create_line(pos, (self.ctr[1]*self.size[1])-5, pos, (self.ctr[1]*self.size[1])+5,\
                                       fill=self.mark_color, tag='marks')
                else:
                    self._c.create_line((self.ctr[0]*self.size[0])-5, pos, (self.ctr[0]*self.size[0])+5, pos,\
                                       fill=self.mark_color, tag='marks')
                pos += self.mark_stp*self.scale[count]

        #X axis and Y axis lines
        self._c.create_line(0, (self.ctr[1]*self.size[1]), self.size[0],\
                            (self.ctr[1]*self.size[1]), fill=self.axis_color,\
                            tag='marks')
        self._c.create_line((self.ctr[0]*self.size[0]), self.size[1],\
                            (self.ctr[0]*self.size[0]), 0, fill=self.axis_color,\
                            tag='marks')
        
        #Range marks(in red)
        for count in range(2):
            self._c.create_line(self.rng[count]*self.scale[0]+\
                                (self.ctr[0]*self.size[0]),\
                               (self.ctr[1]*self.size[1])-5,\
                               self.rng[count]*self.scale[0]+\
                                (self.ctr[0]*self.size[0]),\
                               (self.ctr[1]*self.size[1])+5, fill=self.range_color,\
                                tag='marks')
        
        if self._cnt_equations:#redraw the cnt_equations, if there are any.
            for (color, (equ, params)) in self._cnt_equations.items():
                self.graph(equ, params, color) 

    def graph(self, equ='Same as before', params='No user params', color='black'):
        """See the doc string for the a_graph function."""

        #Handle arguments
        if params=='No user params':
            params={}
        if equ=='Same as before':
            if color in self._cnt_equations.keys():
                equ=self._cnt_equations[color]
            else:
                raise Exception, 'No equation given, and no equation with that'\
                      ' color in memory!'
        self._cnt_equations.update({color:(equ, params)})

        for ref in params.values():
            if type(ref) is ListType:
                key=params.keys()[params.values().index(ref)]
                for item in ref:
                    params[key]=item
                    self.graph(equ, params, color)
                    if raw_input('Show next value?') == 'n':
                        return
                return
        #Now, generate the points...
        ans=[]
        x=float(self.rng[0])
        lo=locals(); lo.update(params) #add your params.
        while x <= self.rng[1]:
            try:
                lo.update({'x':x})
                y=eval(equ, globals(), lo)
            except NameError:
#                print sys.exc_info()
                sys.stderr.write('ERROR: Non-defined parameter used in given '\
                      'equation; please re-graph with graph.a_graph and '\
                      'include the parameter.')
                break
            except ZeroDivisionError:
                print 'Division by zero at X:', x, 'Ignoring.'
            except ValueError:
                if sys.exc_info()[1].args[0]=='math domain error':
                        print 'Math Domain Error at X:',0,'Ignoring.'
                else:
                    sys.excepthook(*sys.exc_info())
            except StopIteration:
                print 'No more values avaliable at X:', x, 'Stopping point generation.'
                break

            else:
                ans.append((x*self.scale[0]+(self.ctr[0]*self.size[0]),\
                            -y*self.scale[1]+(self.ctr[1]*self.size[1])))
            x += self.stp

        #And draw them.
        self._c.delete('graph:'+color)
        for count in range(1, len(ans)):
            self._c.create_line(ans[count][0],\
                               ans[count][1],\
                               ans[count-1][0],\
                               ans[count-1][1],\
                               fill=color, tag='graph:'+color)
    def clear(self, color):
        self._c.delete('graph:'+color)
        if color in self._cnt_equations.keys():
            del self._cnt_equations[color]
    def getdoc(self, option):
        return self._option_dict.get(option, 'error')
    def get(self, option):
        if not self._option_dict.has_key(option):
            return 'error'
        return getattr(self, option, 'error')
HELP_TEXT="""
This is a program to draw graphs.  Here is a list of
the functions available, and what they do. Each can be used by
typing graph.<name of function>(<arguments, if any>)  Documentation
on each can be accessed by calling them with no arguments.

    help -- You're looking at it.
    a_graph -- The most important function.  Makes graphs.
    change -- The second most important function.  Allows you to change
        the options for the graphing window (Get a list of options by
        calling it with no args) (Get docs on one option by setting it to None).
    make_interface -- This recreates the PyGraph window.
"""
def help():
    print HELP_TEXT
def clear(color):
    _private_graph_instance.clear(color)
def make_interface():
    """This recreates the PyGraph window if you have accidently deleted it."""
    _private_graph_instance.make_interface()
def get(option):
    tmp=_private_graph_instance.get(option)
    if tmp=='error':
        raise AttributeError, 'This is not a known option! (Call graph.change() for help)'
    return tmp
def a_graph(equation='Same as before', your_params='No user params', color='black'):
    """This makes graphs.

        The equation should be a valid Python expression as a string
    (see Python docs for more info).  The most common trap is that
    exponentiation is spelled '**', so x squared would be spelled 'x**2'.
        The variable 'x' will be defined, but any other variables you use
    should be listed in your_params as dictionary entries(like the dicts
    returned by globals() and locals()).
        The color arg can be given to specify the color of the graph to draw.
    Only one graph of each color will be shown at once."""
    if equation=='Same as before' and your_params=='No user params' and color=='black':
        print '---Documentation for graph.a_graph---'
        print a_graph.__doc__
        return
    return _private_graph_instance.graph(equation, your_params, color)
def change(*args, **keyargs):
    """This changes the graphing window's options.
    
        To get a list of options, call graph.change('options').
    To set an option, enter its name, an equals sign, then
    the value you want to assign to it.  To change more than
    one option, repeat the above, seperating each item by a comma.
    To access help on an option, set it to the word "None"(no quotes)"""
    if args==() and keyargs=={}:
        print '---Documentation for graph.change---'
        print change.__doc__
    tmp=_private_graph_instance.apply_change(*args, **keyargs)
    if type(tmp)==type(''):
        print tmp
    else:
        return tmp
class Sometimes:
    def __init__(self, func, every, closeEnough):
        """Use "func()" to update the return value of "Sometimes.it(n)"
        every time "n" is "closeEnough" to a multiple of "every". """
        self.func=func
        self.every=every
        self.closeEnough=closeEnough
        self.it(self.every)
    def it(self, n):
        if n % self.every < self.closeEnough:
            self.val=self.func()
        return self.val
def main():
    global _private_graph_instance
    _private_graph_instance=Grapher()
    print 'Call graph.help() for info on how to use PyGraph'

if __name__=='__main__':
    print 'This module should be imported, not run.  Please open up a Python shell amd type \'import graph\' to use this program.'
    raw_input('Press any key when done reading...')
else:
    main()
"""Changelog:
*Most of the program was written before I started making numbered versions.
1.0 First numbered version.  Only available without any identification.
1.1 This version.
    Added raw_input to stop the don't import message from vanishing before it
    could be read.
"""
