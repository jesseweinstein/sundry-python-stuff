""" Checklist display program. ver. 1.0

Run it as a script, and give it a file as an
arg.  It will display the lines in the file as selectable items in a list.

I wanted a way to checkoff items in a list, without printing out a copy of a
file, or cumbersomely entering X's at the beginings or ends of each line.
This solves that problem."""

import Tkinter
class ChecklistDisplay:
    """Checklist display.  Given a list of strings, it will display them and let you select and unslect them."""
    def __init__(self, items, title='Checklist'):
        self.root=Tkinter.Tk()
        self.root.title(title)
        self.labels=[]
        self.populate(items)
        self.instruct_lbl=Tkinter.Label(self.root, font=('', '10', ''),\
                                        text='Click to set, third-button'\
                                        ' click to unset.')
        self.instruct_lbl.pack(fill=Tkinter.BOTH)
    def setOnCB(self, event):
        event.widget.config(relief=Tkinter.SUNKEN, bg='red')
    def setOffCB(self, event):
        event.widget.config(relief=Tkinter.RAISED, bg='gray')
    def populate(self, items):
        for item in items:
            self.labels.append(Tkinter.Label(self.root, text=item,\
                                             relief=Tkinter.RAISED,\
                                             bg='gray',\
                                             font=('', '12' '')))
            self.labels[-1].bind('<1>', self.setOnCB)
            self.labels[-1].bind('<3>', self.setOffCB)
            self.labels[-1].pack(fill=Tkinter.BOTH)
def IDLEtest():
    """Returns 1 when IDLE is running, 0 else. """
    import sys
    try:
        if sys.stdin.__module__=='PyShell':
            return 1
        else:
            return 0
    except AttributeError:
        return 0
    #Copied from good_bits.py on 1/16/2003.
    
if __name__=='__main__':
    import sys
    if len(sys.argv)>1:
        app=ChecklistDisplay(open(sys.argv[1]).read().splitlines())
    else:
        app=ChecklistDisplay(['No Items Given!', 'This', 'is', 'just', 'a', 'test!'], 'Test Checklist')
    if not IDLEtest():
        app.root.mainloop()
